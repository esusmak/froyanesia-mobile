<?php

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace'  => 'Web'], function () {
    Route::group(['namespace' => 'Home'], function () {
        Route::get('/', 'HomeController@index')->name('public.home');
        Route::get('/redirect', 'HomeController@redirect')->name('public.redirect');
    });


    Route::group(['middleware' => 'mustAuth'], function () {
        Route::get('/products/{id}', 'ProductController@show')->name('product.show');

        Route::get('/profile', 'UserController@index')->name('profile.index');
        Route::get('/profile/ubah', 'UserController@update')->name('profile.update');
        Route::post('/profile/ubah', 'UserController@updateProcess')->name('profile.update');

        Route::get('/keranjang', 'CartController@index')->name('cart.index');

        Route::get('/voucher/{id}', 'VoucherController@show')->name('voucher.show');

        Route::get('/ambil', 'ChooseOrderController@take')->name('order.take');
        Route::get('/kirim', 'ChooseOrderController@delivery')->name('order.delivery');
        Route::get('/kirim/getRegencies', 'ChooseOrderController@getRegencies')->name('order.getRegencies');
        Route::get('/kirim/getDistrict', 'ChooseOrderController@getDistrict')->name('order.getDistrict');
        Route::get('/kirim/getVillage', 'ChooseOrderController@getVillage')->name('order.getVillage');

        Route::get('/checkout', 'CheckoutController@index')->name('checkout.index');

        Route::get('/transaksi', 'TransactionController@index')->name('transaction.index');
        Route::get('/transaksi/{id}', 'TransactionController@show')->name('transaction.show');
        Route::get('/transaksi/{id}/lacak', 'TransactionController@lacak')->name('transaction.lacak');

        Route::get('/pembayaran/{id}', 'TransactionController@pembayaran')->name('transaction.pembayaran');
        Route::post('/pembayaran/{id}', 'TransactionController@pembayaranProcessed');

        Route::get('/notifikasi', 'NotifikasiController@index');
        Route::get('/notifikasi/{id}', 'NotifikasiController@show');
    });
});


Route::group(['namespace'  => 'Web'], function () {

    // AUTH WEB
    Route::group(['namespace'  => 'Auth'], function () {
        Route::get('/login', 'AuthController@login')->name('public.auth.login');
        Route::post('/login', 'AuthController@loginProcess')->name('public.auth.loginProcess');
        Route::get('/logout', 'AuthController@logout')->name('auth.logout');
        Route::get('/password-baru/{token}', 'AuthController@resetPassword')->name('auth.resetPassword');
        Route::post('/password-baru/{token}', 'AuthController@resetPasswordProcessed');

        Route::get('/reset-password', 'AuthController@forgotPassword')->name('auth.forgotPassword');
        Route::post('/reset-password', 'AuthController@forgotPasswordProcessed');


        Route::get('/register', 'AuthController@register')->name('public.auth.register');
        Route::post('/register', 'AuthController@registerProcess');
        Route::get('/verify-email/{token}', 'AuthController@verifyEmail')->name('public.auth.verifyEmail');

        Route::get('/verification', 'AuthController@verification')->name('auth.verification');
        Route::post('/verification', 'AuthController@verificationProcessed');
        Route::get('/resendcode', 'AuthController@resendcode')->name('public.auth.resendcode');

        Route::get('/auth/{provider}', 'AuthController@authProvider')->name('public.auth.provider');
    });
});

Route::get('/auth/{provider}/callback', 'Web\Auth\AuthController@authProviderCallback')->name('public.auth.providerCallback');

Route::group(['namespace' => 'Web'], function () {
    Route::group(['namespace' => 'Auth'], function () {
        Route::get('/password/reset-password/{token}', 'AuthController@resetPassword')->name('public.auth.resetPassword');
    });
});

Route::get('/bayar-ditempat', function () {
    return view('m-froyanesia.pesanan.cod');
});
Route::get('/privacy-policy', function () {
    return view('m-froyanesia.privacy-policy');
});
