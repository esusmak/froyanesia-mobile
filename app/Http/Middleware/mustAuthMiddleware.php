<?php

namespace App\Http\Middleware;

use App\Helpers\ResponseApi;
use Closure;

class mustAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $message = 'Maaf anda harus login terlebih dahulu';
        if(session()->has('token')) {
            return $next($request);
        }

        return redirect()->route('public.auth.login')->with('message', $message);
    }
}
