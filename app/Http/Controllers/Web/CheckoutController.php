<?php

namespace App\Http\Controllers\Web;

use App\Helpers\ResponseApi;
use App\Http\Controllers\Controller;
use App\Repositories\AccountBankRepository;
use App\Repositories\CartRepository;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    protected $cartRepository;
    protected $accountBankRepository;

    public function __construct(
        CartRepository $cartRepository,
        AccountBankRepository $accountBankRepository
    ) {
        $this->cartRepository = $cartRepository;
        $this->accountBankRepository = $accountBankRepository;
        $this->client = new ResponseApi;
    }

    public function index(Request $request)
    {
        try {
            $token = session()->get('token');
            $carts = $this->cartRepository->all(['include' => ['variant.product.productimages']], ['Authorization' => "Bearer " . $token])->getData();

            $accountBankTransfer = $this->accountBankRepository->all(['group' => 'transfer'], ['Authorization' => "Bearer " . $token])->getData();

            $accountBankLain = $this->accountBankRepository->all(['group' => 'lain'], ['Authorization' => "Bearer " . $token])->getData();
            $transaction = null;
            $body = [
                "type" => "transactions",
                "attributes" => [
                    "voucherId" => "",
                ]
            ];

            $calculate = $this->client->post('v1/transactions/calculate?include=infodiscount', $body, ['Authorization' => "Bearer " . session()->get('token')]);
            // dd($calculate);
            return view('m-froyanesia.pesanan.checkout', compact('carts', 'accountBankTransfer', 'accountBankLain', 'transaction', 'calculate'));
        } catch (\Exception $e) {
            abort(500, $e->getMessage());
        }
    }
}
