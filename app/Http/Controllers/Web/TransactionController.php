<?php

namespace App\Http\Controllers\Web;

use App\Constants\TransactionStatusConst;
use App\Exceptions\CustomException;
use App\Helpers\ResponseApi;
use App\Http\Controllers\Controller;
use App\Repositories\AccountBankRepository;
use App\Repositories\TransactionRepository;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    private $transactionRepository;
    private $accountBankRepository;

    public function __construct(
        TransactionRepository $transactionRepository,
        AccountBankRepository $accountBankRepository
    ) {
        $this->transactionRepository = $transactionRepository;
        $this->accountBankRepository = $accountBankRepository;
    }

    public function index(Request $req)
    {
        $token = session()->get('token');
        if (isset($req->status)) {
            if ($req->status == 'canceled') {
                $transactions = $this->transactionRepository->all(["sort" => 'desc', 'filter' => 'canceled', 'include' => 'paymentmethod'], ['Authorization' => "Bearer " . session()->get('token')]);
            } else {
                $transactions = $this->transactionRepository->all(["sort" => 'desc', 'filter' => $req->status, 'include' => 'paymentmethod'], ['Authorization' => "Bearer " . session()->get('token')]);
            }
        } else {
            $transactions = $this->transactionRepository->all(["sort" => 'desc', 'include' => 'paymentmethod'], ['Authorization' => "Bearer " . session()->get('token')]);
        }

        return view('m-froyanesia.transaksi.index', compact('transactions'));
    }

    public function show(Request $request, $id)
    {
        $params = [
            'include' => [
                "transactiondetails.variant.product",
                "transporter.user",
                "bank",
                "voucher",
                "outlet",
                'buyer',
                'paymentmethod',
                'infodiscount'
            ]
        ];
        $header = [
            'Authorization' => "Bearer " . session()->get('token')
        ];
        $transaction = $this->transactionRepository->find($id, $params, $header)->getData();

        if ($transaction->currentStatus == 'PENDING') {
            return view('m-froyanesia.transaksi.pending', compact(['transaction']));
        } else if ($transaction->currentStatus == 'PAID') {
            return view('m-froyanesia.transaksi.paid', compact(['transaction']));
        } else if ($transaction->currentStatus == 'DELIVERING' || $transaction->currentStatus == 'PROCESSED') {
            return view('m-froyanesia.transaksi.process', compact(['transaction']));
        } else if ($transaction->currentStatus == 'FINISHED') {
            return view('m-froyanesia.transaksi.done', compact(['transaction']));
        } else {
            return view('m-froyanesia.transaksi.canceled', compact(['transaction']));
        }
    }

    public function pembayaran($id)
    {
        try {
            $transaction = $this->transactionRepository->find($id, ['include' => [
                'bank'
            ]], ['Authorization' => "Bearer " . session()->get('token')])->getData();
            $accountBankTransfer = $this->accountBankRepository->all(['group' => 'transfer'], ['Authorization' => "Bearer " . session()->get('token')])->getData();

            $accountBankLain = $this->accountBankRepository->all(['group' => 'lain'], ['Authorization' => "Bearer " . session()->get('token')])->getData();

            if ($transaction->currentStatus != TransactionStatusConst::TRANS_STATUS_PENDING) {
                return redirect()->back()->with('message', 'Maaf, Transaksi sudah di proses');
            }

            return view('m-froyanesia.pesanan.payment', compact('transaction', 'id', 'accountBankTransfer', 'accountBankLain'));
        } catch (\Throwable $th) {
            throw new CustomException(500, 'Gagal Menghubungi Server');
        }
    }

    public function pembayaranProcessed(Request $request, $id)
    {
        try {
            $body = [
                [
                    'name' => 'file',
                    'contents' => fopen($request->file, 'r'),
                    'filename' => $request->file->getClientOriginalName()
                ],
                [
                    'name' => 'personName',
                    'contents' => $request->personName
                ],
                [
                    'name' => 'bankName',
                    'contents' => $request->bankName
                ],
                [
                    'name' => 'bankNo',
                    'contents' => $request->bankNo
                ],
                [
                    'name' => 'transferDate',
                    'contents' => date('Y-m-d H:i:s', strtotime($request->transferDate))
                ]
            ];

            $response = ResponseApi::basePostWithFile("transactions/{$id}/uploadProofOfPayment", $body);

            return redirect()->route('transaction.index')->with('message', 'Upload bukti pembayaran berhasil');
        } catch (\Exception $th) {
            abort(500, $th->getMessage());
        }
    }

    public function lacak($id)
    {
        $params = [
            'include' => ['transporter.user', 'transporter.trackingtransporter','transporter.transportation.transportationtype'],
        ];
        $header = [
            'Authorization' => "Bearer " . session()->get('token')
        ];
        $transaction = $this->transactionRepository->find($id, $params, $header)->getData();
        if (empty($transaction->transporter->trackingtransporter)) {
            return redirect()->back()->with('message', 'Pesananmu masih diproses, mohon ditunggu');
        }
        return view('m-froyanesia.transaksi.lacak', compact('transaction'));
    }
}
