<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Repositories\VoucherRepository;
use App\Repositories\VoucherSelfRepository;
use Illuminate\Http\Request;

class VoucherController extends Controller
{
    private $voucherRepository;

    public function __construct(
        VoucherRepository $voucherRepository
    ) {
        $this->voucherRepository = $voucherRepository;
    }

    public function show($id)
    {
        try {
            $token = session()->get('token');

            $voucher = $this->voucherRepository->find($id, [], ['Authorization' => "Bearer " . session()->get('token')])->getData();

            return view('m-froyanesia.voucher.voucher-detail', compact('voucher'));
        } catch (\Exception $e) {
            abort(500, $e->getMessage());
        }
    }
}
