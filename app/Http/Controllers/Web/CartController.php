<?php

namespace App\Http\Controllers\Web;

use App\Helpers\ResponseApi;
use App\Http\Controllers\Controller;
use App\Repositories\CartRepository;
use App\Repositories\VoucherSelfRepository;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Swis\JsonApi\Client\Interfaces\DocumentClientInterface;

class CartController extends Controller
{
    private $cartRepository;
    private $voucherSelfRepository;
    private $client;

    public function __construct(
        CartRepository $cartRepository,
        VoucherSelfRepository $voucherSelfRepository
    ) {
        $this->cartRepository = $cartRepository;
        $this->voucherSelfRepository = $voucherSelfRepository;
        $this->client = new ResponseApi;
    }

    public function index()
    {
        try {
            $token = session()->get('token');
            $carts = $this->cartRepository->all(['include' => ['variant.product.productimages']], ['Authorization' => "Bearer " . session()->get('token')])->getData();
            $vouchers = $this->voucherSelfRepository->all([], ['Authorization' => "Bearer " . session()->get('token')])->getData();

            $body = [
                "type" => "transactions",
                "attributes" => [
                    "voucherId" => "",
                ]
            ];
            $calculate = $this->client->post('v1/transactions/calculate', $body, ['Authorization' => "Bearer " . session()->get('token')]);

            return view('m-froyanesia.cart.index', compact('carts', 'vouchers', 'calculate'));
        } catch (\Exception $e) {
            abort(500, $e->getMessage());
        }
    }
}
