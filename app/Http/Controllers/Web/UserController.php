<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Session;
use App\Helpers\ResponseApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class UserController extends Controller
{
    public function index()
    {
        $data = ResponseApi::baseGet('user?include=village,role');
        
        if (Session::has('token')) {
            return view('m-froyanesia.profile.index', compact('data'));
        } else {
            return redirect()->route('public.auth.login');
        }
    }

    public function update()
    {
        $data = ResponseApi::baseGet('user?include=village,role');
        return view('m-froyanesia.profile.ubah', compact('data'));
    }

    public function updateProcess(Request $request)
    {
        $body = [
            [
                "name" => "name",
                "contents" => $request->name
            ],
            [
                "name" => "address",
                "contents" => $request->address
            ],
            [
                "name" => "villageId",
                "contents" => $request->villageId
            ],
            [
                "name" => "gender",
                "contents" => $request->gender
            ],
            [
                "name" => "identityNumber",
                "contents" => $request->identityNumber
            ],
            [
                "name" => "bornDate",
                "contents" => $request->bornDate
            ],
        ];

        if (isset($request->password)) {
            array_push($body, [
                "name" => "password",
                "contents" => $request->password
            ]);
        }

        if (isset($request->photo)) {
            array_push($body, [
                "name" => "photo",
                "contents" => fopen($request->photo, 'r'),
                'filename' => $request->file('photo')->getClientOriginalName()
            ]);
        }

        $response = ResponseApi::basePostWithFile('user', $body);

        // session()->put('user', $response);
        if (isset($response['errors'])) {
            return redirect()->back()->withErrors(['msg', $response['errors'][0]['title']]);
        } else {
            return redirect()->route('profile.index')->with(['msg' => 'Profil berhasil diupdate']);
        }
    }
}
