<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Repositories\NotifikasiRepository;
use Illuminate\Http\Request;

class NotifikasiController extends Controller
{
    private $notifikasiRepository;

    public function __construct(
        NotifikasiRepository $notifikasiRepository
    ) {
        $this->notifikasiRepository = $notifikasiRepository;
    }

    public function index()
    {
        try {
            $token = session()->get('token');
            $notification = $this->notifikasiRepository->all(["sort" => 'desc'], ['Authorization' => "Bearer " . session()->get('token')]);
            return view('m-froyanesia.notifikasi.index', compact('notification'));
        } catch (\Exception $e) {
            abort(500, $e->getMessage());
        }
    }

    public function show($id)
    {
        try {
            $token = session()->get('token');
            $notification = $this->notifikasiRepository->find($id, [], ['Authorization' => "Bearer " . session()->get('token')])->getData();
            return view('m-froyanesia.notifikasi.show', compact('notification'));
        } catch (\Exception $th) {
            abort(500, $th->getMessage());
        }
    }
}
