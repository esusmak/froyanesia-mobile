<?php

namespace App\Http\Controllers\Web\Home;

use App\Helpers\ResponseApi;
use App\Http\Controllers\Controller;
use App\Models\ProductItem;
use App\Repositories\CategoryProductRepository;
use App\Repositories\IntroSliderRepository;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Validation\ValidationException;
use Swis\JsonApi\Client\DocumentClient;

class HomeController extends Controller
{
    private $productRepository;
    private $categoryProductRepository;
    private $introSliderRepository;

    public function __construct(
        ProductRepository $productRepository,
        CategoryProductRepository $categoryProductRepository,
        IntroSliderRepository $introSliderRepository
    ) {
        $this->productRepository = $productRepository;
        $this->categoryProductRepository = $categoryProductRepository;
        $this->introSliderRepository = $introSliderRepository;
    }

    public function index(Request $request)
    {
        try {
            if (!empty($request->all())) {
                $params = $this->productRepository->checkParams($request);
                $params['include'] = ['productimages'];

                $products = $this->productRepository->all($params);
            } else {
                $products = $this->productRepository->all(['include' => 'productimages']);
            }
            $categoryProduct = $this->categoryProductRepository->all();
        } catch (\Exception $th) {
            abort(500);
        }

        // set cookie

        if (!$request->cookie('isFirstTime')) {
            $introSlider = $this->introSliderRepository->all(['sort' => 'desc']);
            return response()->view('m-froyanesia.dashboard.intro', compact('introSlider'))->withCookie(cookie()->forever('isFirstTime', true));
        }

        return view('m-froyanesia.dashboard.index', compact('products', 'categoryProduct'));
    }

    public function redirect(Request $request)
    {
        if (isset($request->token)) {
            session()->put('token', $request->token);
            $data = ResponseApi::baseGet('user');
            session()->put('user', $data);
        }
        return redirect()->route('public.home');
    }
}
