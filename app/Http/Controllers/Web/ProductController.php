<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Repositories\CartRepository;
use App\Repositories\ProductPiecesRepository;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $productRepository;
    private $productPiecesRepository;
    private $cartRepository;

    public function __construct(
        ProductRepository $productRepository,
        ProductPiecesRepository $productPiecesRepository,
        CartRepository $cartRepository

    ) {
        $this->productRepository = $productRepository;
        $this->productPiecesRepository = $productPiecesRepository;
        $this->cartRepository = $cartRepository;
    }

    public function show(Request $request, $id)
    {
        $params = [
            'include' => ['category', 'productimages', 'variants.discount'],
        ];
        $headers = [
            'Authorization' => "Bearer " . session()->get('token')
        ];
        try {
            $product = $this->productRepository->find($id, $params)->getData();
            $variant = $product->variants[0];
            $this->productRepository->setEndpoint(
                $this->productRepository->getEndpoint() . "/recommendation"
            );
            $productRecomendation = $this->productRepository->all(['include' => 'productimages']);

            $productPieces = $this->productPiecesRepository->all();
            $myCart = $this->cartRepository->all([], $headers);
            // dd();
            $cart = null;
            foreach ($myCart->getData() as $item) {
                if ($item->variantId == $variant->id) {
                    $cart = $item;
                }
            }
        } catch (\Exception $e) {
            abort(500, $e->getMessage());
        }

        return view('m-froyanesia.details.index', compact(
            'product',
            'cart',
            'variant',
            'productRecomendation',
            'productPieces',
        ));
    }
}
