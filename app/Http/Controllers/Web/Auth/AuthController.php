<?php

namespace App\Http\Controllers\Web\Auth;

use App\Exceptions\CustomException;
use App\Helpers\ResponseApi;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class AuthController extends Controller
{

    public function login()
    {
        return view('m-froyanesia.auth.login');
    }

    public function register()
    {
        return view('m-froyanesia.auth.register');
    }

    public function verification()
    {
        return view('m-froyanesia.auth.verification');
    }

    public function loginProcess(Request $request)
    {
        $body = [
            "type" => "login",
            "attributes" => [
                "emailOrPhone" => $request->emailOrPhone,
                "password" => $request->password
            ]
        ];


        $client = new Client();
        try {
            $req = $client->request('POST', env('BASE_URI_API') . "v1" . '/auth/login', [
                'query' => $body
            ]);
            $res = json_decode($req->getBody()->getContents());
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Nomor atau password salah');
        }


        // TO DO: Error handling
        if (isset($res->errors)) {
            return redirect()->back()->with('message', 'Nomor atau password salah');
        }
        $token = $res->data->attributes->accessToken;
        session()->put('token', $token);
        session()->put('user', $this->getUser($token));
        return redirect()->route('public.home');
    }

    public function getUser($token)
    {
        $response = ResponseApi::baseGet('user');
        if ($response['attributes']['email_verified_at'] == null) {
            $message = "Maaf akun anda belum terverifikasi";
            return redirect()->route('public.auth.login')->with('message', $message);
        }
        return $response;
    }

    public function registerProcess(Request $request)
    {
        $body = [
            'type' => 'registers',
            "attributes" => [
                "name" => $request->name,
                "emailOrPhone" => $request->emailOrPhone,
                "password" => $request->password,
                "villageId" => $request->villageId,
                "address" => $request->address
            ]
        ];

        // $res = Utils::makeRequest(route('api.public.auth.register'), 'POST', $body);
        $client = new Client();
        try {
            $req = $client->request('POST', env('BASE_URI_API') . "v1" .  '/auth/register', [
                'query' => $body
            ]);
        } catch (\Exception $th) {
            return redirect()->back()->with('message', 'Maaf, nomor sudah digunakan');
        }

        $res = json_decode($req->getBody()->getContents());

        if (!empty($res->data->attributes->phone)) {
            session()->put('phone', $res->data->attributes->phone);
        }

        if (isset($res->errors)) {
            return redirect()->back()->with('message', 'Email atau nomor sudah ada');
        }


        if (!empty($res->data->attributes->email)) {
            return redirect()->route('public.auth.login')->with('message', 'Register berhasil, silahkan cek email untuk konfirmasi akun');
        } else if (!empty($res->data->attributes->phone)) {
            return redirect()->route('auth.verification')->with('message', 'Register berhasil, silahkan cek kode verifikasi di whatsapp anda');
        }
    }

    public function authProvider($provider)
    {
        $body = [
            "type" => "socialProvider",
            "attributes" => [
                "provider" => "google"
            ]
        ];

        $client = new Client();
        try {
            $req = $client->request('POST', env('BASE_URI_API') . "v1" .  '/auth/google/redirect', [
                'query' => $body
            ]);
            $res = json_decode($req->getBody()->getContents());
            return redirect($res->data->attributes->url);
        } catch (\Exception $e) {
            abort(500, $e->getMessage());
        }
    }

    public function resetPassword($token)
    {
        if (empty($token)) {
            return redirect()->back()->with('message', 'Maaf anda tidak bisa mengakses halaman tersebut.');
        }

        return view('m-froyanesia.auth.new_password', compact('token'));
    }

    public function forgotPassword()
    {
        return view('m-froyanesia.auth.reset-password');
    }

    public function forgotPasswordProcessed(Request $request)
    {
        $client = new Client();
        try {
            $body = [
                "type" => "reset-password",
                "attributes" => [
                    "emailOrPhone" => $request->emailOrPhone
                ]
            ];

            $req = $client->request('POST', env('BASE_URI_API') . 'v1/auth/reset-password', [
                'query' => $body
            ]);
            $res = json_decode($req->getBody()->getContents());

            return redirect()->route('public.auth.login')->with('message', 'Link reset password berhasil di kirim');
        } catch (\Exception $e) {
            abort(500, $e->getMessage());
        }
    }

    public function resetPasswordProcessed(Request $request, $token)
    {
        $client = new Client();
        try {
            $body = [
                "type" => "change-password",
                "attributes" => [
                    "emailOrPhone" => $request->emailOrPhone,
                    "newPassword" => $request->newPassword
                ]
            ];

            $req = $client->request('POST', env('BASE_URI_API') . "v1/auth/reset-password/$token/change-password", [
                'query' => $body
            ]);
            $res = json_decode($req->getBody()->getContents());
            return redirect()->route('public.auth.login')->with('message', 'Reset password berhasil');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Gagal Menghubungi Server');
        }
    }

    public function verifyEmail($token)
    {
        return view('m-froyanesia.auth.verify-email');
    }

    public function logout()
    {
        session()->flush();
        return redirect()->route('public.auth.login');
    }

    public function verificationProcessed(Request $request)
    {
        $client = new Client();
        try {
            $code = $request->digit1 . $request->digit2 . $request->digit3 . $request->digit4;
            $body = [
                "type" => "checkVerificationCode",
                "attributes" => [
                    "digit" => $code
                ]
            ];

            $req = $client->request('POST', env('BASE_URI_API') . "v1/auth/checkVerificationCode", [
                'query' => $body
            ]);
            $res = json_decode($req->getBody()->getContents());
            return redirect()->route('public.auth.login')->with('message', 'Akun berhasil dikonfirmasi, silahkan login');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Gagal Menghubungi Server');
        }
    }

    public function resendcode(Request $request)
    {
        $client = new Client();
        $body = [
            'type' => 'resendCode',
            "attributes" => [
                "emailOrPhone" => $request->emailOrPhone,
            ]
        ];
        try {
            $req = $client->request('POST', env('BASE_URI_API') . "v1" . '/auth/resendcode', [
                'query' => $body
            ]);
            $res = json_decode($req->getBody()->getContents());
            dd($res);
            return redirect()->back()->with('message', 'Kode sudah dikirim');
        } catch (ClientException $e) {
            return redirect()->back()->with('message', 'Gagal menghubungi server');
        }
    }
}
