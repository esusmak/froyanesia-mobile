<?php

namespace App\Http\Controllers\Web;

use App\Exceptions\CustomException;
use App\Helpers\ResponseApi;
use App\Http\Controllers\Controller;
use App\Repositories\DeliveryTimeRepository;
use App\Repositories\DistrictRepository;
use App\Repositories\ProvinceRepository;
use App\Repositories\RegencyRepository;
use App\Repositories\UserRepository;
use App\Repositories\VillageRepository;
use Exception;
use Illuminate\Http\Request;

class ChooseOrderController extends Controller
{
    private $deliveryTimeRepository;
    private $provinceRepository;
    private $districtRepository;
    private $regencyRepository;
    private $userRepository;
    private $client;

    public function __construct(
        DeliveryTimeRepository $deliveryTimeRepository,
        ProvinceRepository $provinceRepository,
        RegencyRepository $regencyRepository,
        VillageRepository $villageRepository,
        DistrictRepository $districtRepository,
        UserRepository $userRepository
    ) {
        $this->deliveryTimeRepository = $deliveryTimeRepository;
        $this->provinceRepository = $provinceRepository;
        $this->regencyRepository = $regencyRepository;
        $this->districtRepository = $districtRepository;
        $this->villageRepository = $villageRepository;
        $this->userRepository = $userRepository;
        $this->client = new ResponseApi;
    }

    public function take()
    {
        return view('m-froyanesia.ambil-produk.ambil');
    }

    public function delivery()
    {
        try {
            $deliveryTime = $this->deliveryTimeRepository->all(['limit' => 20]);
            $provinces = $this->provinceRepository->all(['sort' => 'asc', 'limit' => 40]);
            $user = $this->client->get("v1/user?include=village", ['Authorization' => "Bearer " . session()->get('token')]);

            // check if village user has ongkir
            if (!empty($user->data->relationships)) {
                $deliveryCostId = $this->client->get("v1/deliverycosts/self?villageId={$user->data->relationships->village->data->id}")->data->id;
            } else {
                $deliveryCostId = 1;
            }
        } catch (\Exception $th) {
            throw new CustomException(500, 'Gagal menghubungi server');
        }
        return view('m-froyanesia.ambil-produk.kirim', compact(
            'deliveryTime',
            'provinces',
            'user',
            'deliveryCostId'
        ));
    }

    public function getRegencies(Request $request)
    {
        try {
            $provinceId = $request->provinceId;
            $regencies = $this->regencyRepository->all(['provinceId' => $provinceId, 'limit' => 99]);
            return response()->json($regencies);
        } catch (\Exception $th) {
            throw new CustomException(500, 'Gagal menghubungi server');
        }
    }

    public function getDistrict(Request $request)
    {
        try {
            $regencyId = $request->regencyId;
            $districts = $this->districtRepository->all(['regencyId' => $regencyId, 'limit' => 99]);
            return response()->json($districts);
        } catch (\Exception $th) {
            throw new CustomException(500, 'Gagal menghubungi server');
        }
    }

    public function getVillage(Request $request)
    {
        try {
            $districtId = $request->districtId;
            $villages = $this->villageRepository->all(['districtId' => $districtId, 'limit' => 99]);
            return response()->json($villages);
        } catch (\Exception $th) {
            throw new CustomException(500, 'Gagal menghubungi server');
        }
    }
}
