<?php
//app/Helpers/Envato/User.php
namespace App\Helpers;

use Exception;
use GuzzleHttp\Client;
use Swis\JsonApi\Client\Client as SwonClient;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Validation\ValidationException;

class ResponseApi
{
    private $client;

    public function __construct()
    {
        $this->client = new SwonClient();
        $url = env('BASE_URI_API');
        $this->client->setBaseUri($url);
    }

    public function get($endpoint, array $headers = [])
    {
        try {
            $response = $this->client->get($endpoint, $headers)->getBody()->getContents();
            return json_decode($response);
        } catch (\Exception $e) {
            throw new Exception('Gagal menghubungi server', 422);
        }
    }

    public function post($endpoint, $body = '', array $headers = [])
    {
        $body = json_encode($body);
        try {
            $response = $this->client->post($endpoint, $body, $headers)->getBody()->getContents();
        } catch (\Exception $e) {
            throw new Exception('Gagal menghubungi server', 422);
        }
        $response = json_decode($response);

        if (isset($response->errors)) {
            $title = $response->errors[0]->title;
            throw new Exception($title, 422);
        }
        return $response;
    }

    /**
     * @param int $user_id User-id
     *
     * @return string
     */

    static function baseGet($uri)
    {
        $base_url = env('BASE_URI_API') . "v1/";
        $url = $base_url . $uri;
        $client = new Client();
        try {
            $request = $client->request('get', $url, [
                'headers' => ['Authorization' => 'Bearer ' . session('token')]
            ]);
            $response = $request->getBody()->getContents();
            $data = json_decode($response, true)['data'];
            return $data;
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }

    static function basePost($url, $form = [])
    {
        $base_url = env('BASE_URI_API') . "v1/";
        if ($url != $base_url) {
            $url = $base_url . $url;
        }

        $client = new Client();
        try {
            $request = $client->request('post', $url, [
                'headers' => ['Authorization' => 'Bearer ' . session('token')],
                'form_params' => $form
            ]);
            // $request = $client->get($url, [
            //     'headers'=> ['Authorization' => 'Bearer '.session('token')]
            // ]);
            $response = $request->getBody()->getContents();
            $data = json_decode($response, true)['data'];
            return $data;
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }

    static function basePostWithFile($url, $form = [])
    {
        $base_url = env('BASE_URI_API') . "v1/";
        if ($url != $base_url) {
            $url = $base_url . $url;
        }

        $client = new Client();
        try {
            $request = $client->request('post', $url, [
                'headers' => ['Authorization' => 'Bearer ' . session('token')],
                'multipart' => $form
            ]);
            $response = $request->getBody()->getContents();

            $data = json_decode($response, true)['data'];

            return $data;
        } catch (\Exception $exception) {
            $responseBody = $exception->getMessage();
            return abort(500);
        }
    }
}
