<?php

namespace App\Repositories;

use Swis\JsonApi\Client\Interfaces\ClientInterface;
use Swis\JsonApi\Client\Repository;

class VariantRepository extends Repository
{
    protected $endpoint = 'v1/variants';
}
