<?php

namespace App\Repositories;

use Swis\JsonApi\Client\Repository;

class NotifikasiRepository extends Repository
{
    protected $endpoint = 'v1/notifications';
}
