<?php

namespace App\Repositories;

use Swis\JsonApi\Client\Repository;

class ProductRepository extends Repository
{
    protected $endpoint = 'v1/products';

    public function checkParams($request)
    {
        $wheres = [];
        foreach ($request->all() as $key => $value) {
            $wheres[$key] = $value;
        }
        return $wheres;
    }

    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }
}
