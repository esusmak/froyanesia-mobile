<?php

namespace App\Repositories;

use Swis\JsonApi\Client\Repository;

class AccountBankRepository extends Repository
{
    protected $endpoint = 'v1/accountBank';
}
