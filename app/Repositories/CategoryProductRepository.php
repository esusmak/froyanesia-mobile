<?php

namespace App\Repositories;

use Swis\JsonApi\Client\Repository;

class CategoryProductRepository extends Repository
{
    protected $endpoint = 'v1/categories';
}
