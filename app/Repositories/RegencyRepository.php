<?php

namespace App\Repositories;

use Swis\JsonApi\Client\Interfaces\ClientInterface;
use Swis\JsonApi\Client\Repository;

class RegencyRepository extends Repository
{
    protected $endpoint = 'v1/regencies';
}
