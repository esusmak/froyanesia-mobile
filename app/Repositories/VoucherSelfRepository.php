<?php

namespace App\Repositories;

use Swis\JsonApi\Client\Interfaces\ClientInterface;
use Swis\JsonApi\Client\Repository;

class VoucherSelfRepository extends Repository
{
    protected $endpoint = 'v1/vouchers/self';
}
