<?php

namespace App\Repositories;

use Swis\JsonApi\Client\Repository;

class UserRepository extends Repository
{
    protected $endpoint = 'v1/user';
}
