<?php

namespace App\Repositories;

use Swis\JsonApi\Client\Interfaces\ClientInterface;
use Swis\JsonApi\Client\Repository;

class TransactionRepository extends Repository
{
    protected $endpoint = 'v1/transactions';
}
