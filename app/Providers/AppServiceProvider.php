<?php

namespace App\Providers;

use App\Helpers\ResponseApi;
use App\Repositories\CartRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
            if (session()->has('token')) {
                $carts = ResponseApi::baseGet('carts');
            
                if(count($carts ?? []) > 0){
                    $countCart = $this->_loopCountCart($carts);
                }else{
                    $countCart = 0;
                }

                $view->with('countCart', $countCart);
                // $view->with('countNotification', $notification['notificationStats']['unread'] ?? []);
            } else {
                $view->with('countCart', 0);
                // $view->with('countNotification', 0);
            }
        });
    }

    private function _loopCountCart($carts, $countCart = 0,$index = 0){
        $countCart += $carts[$index]['attributes']['qty'];

        if($index == (sizeof($carts)-1))
        {
            return $countCart;
        } 
        else{
            return $this->_loopCountCart($carts, $countCart, $index+1);
        }
    }
}
