$(window).on('load', () => {
    const preloader = $('.preloader')
    if ( !sessionStorage.isVisited ) {
        sessionStorage.isVisited = 'true';
        preloader.show().delay(2500).fadeOut("slow")
        $("body").delay(350).css({ overflow: "visible" });
    } else {
        preloader.hide()
    }
})
$(document).ready( () => {

    $(".kategori-slider").slick({
        slidesToShow: 4,
        slidesToScroll: 3,
        dots: false,
        autoplay: false,
        arrows: false,
        infinite: false,
        responsive: [
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 2,
              }
            },
            {
                breakpoint: 350,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1,
                }
              }
          ]
    });
    $(".recommend-slider").slick({
        slidesToShow: 3,
        slidesToScroll: 2,
        dots: false,
        autoplay: false,
        infinite: true,
        nextArrow: '<i class="fa-lg fas fa-chevron-right arrow-next"></i>',
        responsive: [
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
              }
            }
          ]
    });

    $('#filter-transaksi').slick({
        slidesToShow: 4,
        slidesToScroll: 3,
        dots: false,
        autoplay: false,
        arrows: false,
        infinite: false,
        variableWidth: true,
        responsive: [
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 2,
              }
            },
            {
                breakpoint: 350,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1,
                }
              }
          ]
    })

    $('#image-slider').slick({
        dots: true,
        fade: true,
        cssEase: 'linear',
        slidesToShow: 1,
        adaptiveHeight: true,
        prevArrow: '<i class="fa-lg fas fa-chevron-left arrow-prev"></i>',
        nextArrow: '<i class="fa-lg fas fa-chevron-right arrow-next"></i>',
    });

    $.extend(true, $.fn.datetimepicker.defaults, {
        icons: {
          time: 'far fa-clock',
          date: 'far fa-calendar',
          up: 'fa fa-arrow-up',
          down: 'fa fa-arrow-down',
          previous: 'fas fa-chevron-left',
          next: 'fas fa-chevron-right',
          today: 'far fa-calendar-check-o',
          clear: 'far fa-trash',
          close: 'far fa-times'
        }
    })
    const today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    $('#tgl_transfer').datetimepicker({
        minDate: moment().add(1, 'h'),
        format: 'DD-MM-YYYY HH:mm:ss'
    });
    $('#date-pick').datetimepicker({
        minDate: today,
        format: 'DD-MM-YYYY'
    });
    $('#date').datetimepicker({
        minDate: today,
        format: 'DD-MM-YYYY'
    });
    $('#time').datetimepicker({
        minDate: moment().add(1, 'h'),
        format: 'HH:mm:ss'
    });
    $('#pick-birthday').datetimepicker({
        minDate: today,
        format: 'DD-MM-YYYY'
    });

    const verification = () => {
        $('.digit-group').find('input').each(function () {
            $(this).attr('maxlength', 1);
            $(this).on('keyup', function (e) {
                var parent = $($(this).parent());
    
                if (e.keyCode === 8 || e.keyCode === 37) {
                    var prev = parent.find('input#' + $(this).data('previous'));
    
                    if (prev.length) {
                        $(prev).select();
                    }
                } else if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
                    var next = parent.find('input#' + $(this).data('next'));
    
                    if (next.length) {
                    $(next).select();
                    }
                }
            });
        });
    }
    verification()
    const disable = () => {
        const register = $('.form-register .input-custom'),
        submitRegister = $('#submit-register'),
        verification = $('.input-code'),
        submitverification = $('.verify')

        register.on('keyup', function() {
            let empty = false;
        
            register.each( function() {
              empty = $(this).val().length == 0;
            });
            empty ? submitRegister.prop("disabled", true) : submitRegister.prop('disabled', false)
            
        });
        verification.on('keyup', function() {
            $(this).val().length == 0 ? verification.css("border-color", "#b9b7b7") : verification.css("border-color", "#c96370")
            let none = false;

            verification.each( function() {
                none = $(this).val().length == 0;
            });
            none ? submitverification.prop("disabled", true) : submitverification.prop('disabled', false)
        });

    }
    disable()
    $('.input-code').each(()=> {
        $(this).val() == '' ? $(this).css('border-color', '#B9B7B7') : $(this).css('border-color', '#C96370')
    })

    $(document).click((event) => {
        var click = $(event.target);
        var _open = $(".navbar-collapse").hasClass("show");
        if (_open === true && !click.hasClass("navbar-toggler")) {
            $(".navbar-toggler").click();
        }
    });
    
    if($('body .bot-nav')[0]){
        $('main').css('padding-bottom', '4rem')
    }
    if($('body footer #total')[0]){
        $('main').css('padding-bottom', '16rem')
    }
    if($('body #recommend')[0]){
        $('main').css('padding-bottom', '7rem')
    }
    if($('body .transaksi-action')[0]) {
        $('main').css('padding-bottom', '10rem')
    }
    if($('body header')[0]) {
        $('main').css("padding-top", "75px")
    }
    if($('body #listNotifikasi')[0]) {
        $('main').css("padding-top", "9rem")
    }

    const activeClass = () => {
        $('.bot-nav li a').filter(function(){
            return this.href==location.href
        }).parent().addClass('active').siblings().removeClass('active')
        $('.ambil-kirim li a').filter(function(){
            return this.href==location.href
        }).parent().addClass('active').siblings().removeClass('active')
    }
    activeClass()
    
    $(".voucher-data").on("blur", function() {
        $(this).next().fadeOut('medium');
      })
    $(".voucher-data").on("focus", function() {
        $(this).next().fadeIn('medium');
    })

    const cutText = () => {
        $('.voucher-list .name-voucher').each( function() {
            if($(this).html() > 15) {
                $(this).html($(this).html().substring(0, 13) + '...');
            }
        })
        $('#product .item-intro').each( function() {
            if($(this).html() > 25) {
                $(this).html($(this).html().substring(0, 23) + '...');
            }
        })
        $('#listNotifikasi .content-list-notifikasi').each( function() {
            if($(this).html() > 50) {
                $(this).html($(this).html().substring(0, 48) + '...');
            }
        })
    }
    cutText()

    const hide = () => {
        const text = $('#text-password').text().toString().split("")
        let data = []
        text.forEach(x => {
            data.push(x.replace(/[a-z]/, '\u25CF'))
            
        })
        $('#text-password').text(data.join(""))

        $("#toggle-password").click(function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
              input.attr("type", "text");
            } else {
              input.attr("type", "password");
            }
        });
    }
    hide()

    
})
// mapboxgl.accessToken = 'pk.eyJ1IjoiYXJhZGVhdGV0c3V5YSIsImEiOiJja2ttZnBvaGswdTJqMm9wZGhhYmx4dXJ6In0.Vp-j_x4zDDKqDXNUy_jbIQ';
// navigator.geolocation.getCurrentPosition(
//     successLocation,
//     errorLocation,{
//         enableHighAccuracy: true
//     })
// function successLocation(position) {
//     setupMap([position.coords.latitude, position.coords.longitude])
// }
// function errorLocation() {
//     setupMap([-2.24, 53.48])
// }
// function setupMap(center) {
//     const map = new mapboxgl.Map({
//         container: 'map',
//         style: 'mapbox://styles/mapbox/streets-v11',
//         center: center,
//         zoom: 14
//     }); 
    
//     const nav = new mapboxgl.NavigationControl();
//     map.addControl(nav);

//     var directions = new MapboxDirections({
//         accessToken: mapboxgl.accessToken
//     });  
//     map.addControl(directions, 'top-left');
// }