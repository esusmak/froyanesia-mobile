<h4 class="detail-name mb-3">{{$product->name}}</h4>
<p class="description">{{$product->description}}</p>
<div class="row p-2" id="list-variant">
    @foreach($product->variants as $item)
    <div class="col-6 px-2 mb-3">
        <a href="#!" class="variant" data-variant_id="{{$item->id}}">
            <div class="card p-2 text-center">
                <h5 class="detail-variant">Rp {{number_format($item->price)}}/<span>{{$item->unitValue . ' ' . $item->unitTypeName}}</span></h5>
            </div>
        </a>
    </div>
    @endforeach
    <div class="col-12 px-2 mt-3" id="confirm">
        <button class="btn-confirm" disabled>Tambah</button>
    </div>
</div>

@push('customJs')
<script>
    $('#list-variant').on('click', '.variant', function() {
        const variant_id = $(this).data('variant_id')
        $('.btn-confirm').attr('data-variant_id', variant_id)
        $('.btn-confirm').attr('disabled', false)
    })

    $('#confirm').on('click', '.btn-confirm', function() {
        const variant_id = $(this).data('variant_id')
        let body = {
            "type": "carts",
            "attributes": {
                "variantId": variant_id,
                "qty": 1
            }
        }
        
        $.ajax({
            url: baseApi + "v1/carts",
            method: "POST",
            headers: {
                'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
            },
            data: body,
            success: function(data) {
                let res = data
                Swal.fire({
                    title: res.meta.message,
                    icon: 'info',
                    confirmButtonText: 'Ok'
                })
                let currentCount = `{!! $countCart !!}`
                $('.cart-counter').text(parseInt(currentCount) + 1 )
            },
            error: function(request, textStatus, errorThrown) {
                let res = JSON.parse(request.responseText)
                    let msg = res.errors[0]
                    Swal.fire({
                        title: msg.title,
                        icon: 'error',
                        confirmButtonText: 'Ok'
                    })
                location.reload()
            }
        })
    })
</script>
@endpush