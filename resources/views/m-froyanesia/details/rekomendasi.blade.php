<div id="recommend" class="container">
    <h5 class="recommend-title my-4">Produk Lainnya</h5>
    <div class="recommend-slider mt-4">
        @foreach($productRecomendation->getData() as $item)
            <div class="h-100 py-3">
                <a href="{{$item->cheapestStock == 0 ? "stock-none" : route('product.show', $item->id)}}">
                    <div class="card">
                        <div class="top-card">
                            <figure class="recommend-img">
                                <img src="{{!empty($item->productimages) ? $item->productimages[0]->file : ''}}" alt="{{$item->name}}" class="img-fluid">
                            </figure>
                        </div>
                        <div class="bot-card">
                            <h6 class="recommend-name mb-2">{{$item->name}}</h6>
                            @if ($item->isDiscount)
                                <div class="d-flex flex-row align-items-center">
                                    <p class="recommend-persen mb-0 mr-2">-20%</p>
                                    <p class="recommend-coret mb-0">Rp {{number_format($item->priceBeforeDiscount)}}</p>
                                </div>
                            @endif
                            <p class="recommend-price mt-2">Rp {{number_format($item->cheapestPrice)}}</p>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
</div>