<h3 class="detail-name mb-3">{{$product->name}}</h3>

<p class="description">{{$product->description}}</p>
<section class="information-detail py-4">
    <div class="d-flex justify-content-between mb-2">
        <div class="text-left">
            <h5>Berat :</h5>
        </div>
        <div class="text-right">
            <p>{{$variant->unitValue . " ". $variant->unitTypeName}}</p>
        </div>
    </div>
    <div class="d-flex justify-content-between mb-2">
        <div class="text-left">
            <h5>Harga :</h5>
        </div>
        <div class="text-right">
            <p>
                @if($product->isDiscount)
                <span class="discount-price pr-2">Rp {{number_format($product->priceBeforeDiscount)}}</span> Rp {{number_format($product->cheapestPrice)}}
                @else
                Rp {{number_format($product->cheapestPrice)}}
                @endif
            </p>
        </div>
    </div>
    <div class="d-flex justify-content-end">

            @if($product->isDiscount)
            <p class="discount-value">
                {{-- Beli 5 Gratis 1! --}}
                -
                {{$variant->discount->discount}}
                %  
            </p>
            @endif
    </div>

    {{-- <div class="d-flex justify-content-end">
        <p class="discount-value">hemat 24%</p>
    </div> --}}
</section>
<div class="variant-group">
    <select name="isPieces" id="isPieces" data-id="{{$cart->id ?? null}}" class="input-custom " {{$product->isPieces ? "" : "disabled"}}>
        <option selected disabled>Pilih potong</option>

        @foreach ($productPieces->getData() as $item)
        <option @if(!empty($cart->pieces))
            {{$cart->pieces == $item->pieces ? "selected" : ''}}
            @endif
            value="{{$item->id}}">{{$item->pieces}}
        </option>
        @endforeach
    </select>
    <div class="group-qty">
        <button class="btn-minus" data-id="{{$cart->id ?? null}}">
            <span>-</span>
        </button>
        <input class="input-qty" type="text" value="{{$cart->qty ?? 0}}">
        <button class="btn-plus">
            <span>+</span>
        </button>
    </div>

</div>
<div class="col-12 px-2 mt-5" id="confirm">
    <a href="{{route('cart.index')}}">
        <button class="btn-confirm">Lihat Keranjang</button>
    </a>
</div>

@push('customJs')
<script>
    const qtyParent = $('.input-qty')
    const variantId = `{!! $variant->id !!}`
    let qty = qtyParent.val();
    // console.log("qty awal "+qty)
    if (qty == 0) {
        $('.btn-minus').attr('disabled', true)
    }

    $(".btn-plus").on('click', function(e) {
        e.preventDefault();
        $('.btn-plus').attr('disabled', true)
        $('.btn-minus').attr('disabled', true)
        qty = parseInt(qty) + 1;
        // console.log("qty ditambah "+qty)

        let body = {
            "type": "carts",
            "attributes": {
                "variantId": variantId,
                "qty": 1
            }
        }
        const isPieces = $('#isPieces')
        if(isPieces.val() != null) {
            body['attributes']['productpieceId'] = isPieces.val();
        }
        
        $.ajax({
            url: `${baseApi}v1/carts`,
            headers: {
                'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
            },
            method: 'POST', 
            data: body,
            success: function( data ) {
                qtyParent.val(qty)
                let msg = "item berhasil ditambahkan ke keranjang belanja"
                Swal.fire({
                    icon: 'success',
                    title: `${msg}`,
                    showConfirmButton: false,
                    timer: 1500,
                    width :400
                })
                var countCart = $('#countCart').text()
                $('#countCart').text(parseInt(countCart)+1)
                $('.btn-plus').attr('disabled', false)
                $('.btn-minus').attr('disabled', false)
                // location.reload()
                if(countCart == ""){
                  location.reload()
                }
            }, error: function(request, textStatus, errorTh) {
                const res = JSON.parse(request.responseText)
                if(res.errors) {
                    const msg = res.errors[0].title
                    flashAlert(msg, 'error')
                }
            }
        })
    })

    $(".btn-minus").on('click', function(e) {
        const cartId = $(this).data('id')
        e.preventDefault();
        $('.btn-plus').attr('disabled', true)
        $('.btn-minus').attr('disabled', true)
        if (qty <= 1) {
            if (confirm("Hapus produk dari keranjang belanja ?")) {
                let cart_id = $(this).data('id')
                $('.qty-minus').attr('disabled', true)
                $('.qty-plus').attr('disabled', true)
                deleteCart(cart_id)
            }
            return false
        }
        qty = parseInt(qty) - 1;
        let body = {
            "type": "carts",
            "attributes": {
                "variantId": variantId,
                "qty": qty
            }
        }

        const isPieces = $('#isPieces')
        if (isPieces.val() != null) {
            body['attributes']['productpieceId'] = isPieces.val();
        }

        $.ajax({
            url: `${baseApi}v1/carts/${cartId}`,
            headers: {
                'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
            },
            method: 'PUT',
            data: body,
            success: function(data) {
                qtyParent.val(qty)
                let msg = "item telah berkurang di keranjang belanja"
                Swal.fire({
                    icon: 'success',
                    title: `${msg}`,
                    width: '400px',
                    showConfirmButton : false
                })
                var countCart = $('#countCart').text()
                $('#countCart').text(parseInt(countCart)-1)
                $('.btn-plus').attr('disabled', false)
                $('.btn-minus').attr('disabled', false)
                
            },
            error: function(request, textStatus, errorTh) {
                const res = JSON.parse(request.responseText)
                if (res.errors) {
                    const msg = res.errors[0].title
                    flashAlert(msg, 'error')
                }
            }
        })
    })

    // $('#isPieces').change(function() {
    //     const cartId = $(this).data('id')
    //     let body = {
    //         "type": "carts",
    //         "attributes": {
    //             "variantId": variantId,
    //             "qty": qty
    //         }
    //     }

    //     const isPieces = $('#isPieces')
    //     if (isPieces.val() != null) {
    //         body['attributes']['productpieceId'] = isPieces.val();
    //     }

    //     $.ajax({
    //         url: `${baseApi}v1/carts/${cartId}`,
    //         headers: {
    //             'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
    //         },
    //         method: 'PUT',
    //         data: body,
    //         success: function(data) {
    //             qtyParent.val(qty)
    //             let msg = data.meta
    //             Swal.fire({
    //                 title: `${msg.message}`,
    //                 icon: 'success',
    //                 confirmButtonText: "ok"
    //             })
    //             // location.reload()
    //         },
    //         error: function(request, textStatus, errorTh) {
    //             const res = JSON.parse(request.responseText)
    //             if (res.errors) {
    //                 const msg = res.errors[0].title
    //                 flashAlert(msg, 'error')
    //             }
    //         }
    //     })
    // })

    function deleteCart(cart_id) {
        $.ajax({
            url: baseApi + "v1/carts/" + cart_id,
            method: "DELETE",
            headers: {
                'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
            },
            success: function(data) {
                location.reload()
            }
        })
    }
</script>
@endpush