@extends('m-froyanesia.layouts.master')
@section('page')
    Detail Produk
@endsection
@section('content')
    <header id="detail">
        <nav class="navbar">
            <div class="container px-3 py-2">
                <div class="d-flex flex-row align-items-center w-100">
                    <button class="navbar-toggler p-0 m-0 mr-4 d-flex flex-row align-items-center " type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon p-0">
                            <img src="{{asset('assets/image/Icons/Main Menu.svg')}}" alt="Menu" class="img-dashboard">
                        </span>
                    </button>
                    <form action="{{route('public.home')}}" class="form-search">
                        <div class="card">
                                <input type="text" name="search" placeholder="Cari">
                                <img src="{{asset('assets/image/Icons/Search.svg')}}" alt="Search" class="img-search">
                        </div>
                    </form>
                </div>
            </div>
            @include('m-froyanesia.partials.nav-beranda')
        </nav>
    </header>
    <main>
        <article id="detail">
            <section id="image-slider">
                @if($product->productimages)
                    @foreach ($product->productimages as $item)
                    <div>
                        <figure class="text-center">
                            <img src="{{$item->file}}" alt="{{$product->name}}" class="w-100">
                        </figure>
                    </div>
                    @endforeach
                @endif
            </section>
            <section id="information" class="p-3 pt-sm-0 pb-sm-4 px-sm-4">
                <div class="container">
                    @include('m-froyanesia.details.variant')
                </div>
            </section>
            @include('m-froyanesia.details.rekomendasi', ['productRecomendation' => $productRecomendation])
        </article>
    </main>
    <footer>
        @include('m-froyanesia.partials.bot-nav')
    </footer>
@include('m-froyanesia.partials.add-cart-success')
@endsection
