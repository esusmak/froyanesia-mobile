@extends('m-froyanesia.layouts.master')
@section('page')
    Privacy & Policy
@endsection
@section('content')
<header>
    <nav class="navbar navbar-expand">
        <div class="container px-3 py-2">
            <a href="/" class="title-page"><i class="fas fa-chevron-left"></i></a>
            <h6 class="title-page mx-auto">Privacy & Policy</h6>
        </div>
    </nav>
</header>
<main>
    <article class="privacy-policy text-justify mt-3">
        <div class="container">
            <section>
                <p class="privacy-policy-content">
                    This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable Information' (PII) is being used online. PII, as described in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.
                </p>
            </section>
            <section class="mt-4">
                <h6 class="privacy-policy-subtitle">
                    <strong>What personal information do we collect from the people that visit our website?</strong>
                </h6>
                <p class="privacy-policy-content">When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, mailing address, phone number or other details to help you with your experience.</p>
            </section>
            <section class="mt-4">
                <h6 class="privacy-policy-subtitle">
                    <strong>When do we collect information?</strong>
                </h6>
                <p class="privacy-policy-content">We collect information from you when you register on our site, place an order, subscribe to a newsletter, respond to a survey, fill out a form or enter information on our site, provide us with feedback on our products or services.</p>
            </section>
            <section class="mt-4">
                <h6 class="privacy-policy-subtitle">
                    <strong>How do we use your information?</strong>
                </h6>
                <p class="privacy-policy-content">We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:</p>
                <ul class="py-0 px-3">
                    <li>To personalize your experience and to allow us to deliver the type of content and product offerings in which you are most interested.</li>
                    <li>To improve our website in order to better serve you.</li>
                    <li>To allow us to better service you in responding to your customer service requests.</li>
                    <li>To administer a contest, promotion, survey or other site feature.</li>
                    <li>To quickly process your transactions.</li>
                    <li>To ask for ratings and reviews of services or products</li>
                    <li>To follow up with them after correspondence (live chat, email or phone inquiries)</li>
                </ul>
            </section>
            <section class="mt-4">
                <h6 class="privacy-policy-subtitle">
                    <strong>How do we protect your information?</strong>
                </h6>
                <p class="privacy-policy-content">We do not use vulnerability scanning and/or scanning to PCI standards. We only provide articles and information. We never ask for credit card numbers. We do not use Malware Scanning.</p>
                <p class="privacy-policy-content">Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.</p>
                <p class="privacy-policy-content">We implement a variety of security measures when a user places an order enters, submits, or accesses their information to maintain the safety of your personal information. All transactions are processed through a gateway provider and are not stored or processed on our servers.</p>
            </section>
            <section class="mt-4">
                <h6 class="privacy-policy-subtitle">
                    <strong>Do we use 'cookies'?</strong>
                </h6>
                <p class="privacy-policy-content">Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.
                    We use cookies to:</p>
                <ul class="py-0 px-3">
                    <li>Help remember and process the items in the shopping cart.</li>
                    <li>Understand and save user's preferences for future visits.</li>
                    <li>Compile aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future. We may also use trusted third-party services that track this information on our behalf.</li>
                </ul>
                <p class="privacy-policy-content">You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies.</p>
            </section>
            <section class="mt-4">
                <h6 class="privacy-policy-subtitle">
                    <strong>If users disable cookies in their browser:</strong>
                </h6>
                <p class="privacy-policy-content">If you turn cookies off, some features will be disabled, including some of the features that make your site experience more efficient and may not function properly. However, you will still be able to place orders by contacting customer service.</p>
            </section>
            <section class="mt-4">
                <h6 class="privacy-policy-subtitle">
                    <strong>Third-party disclosure</strong>
                </h6>
                <p class="privacy-policy-content">We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information unless we provide users with advance notice. This does not include website hosting partners and other parties who assist us in operating our website, conducting our business, or serving our users, so long as those parties agree to keep this information confidential. We may also release information when it's release is appropriate to comply with the law, enforce our site policies, or protect ours or others' rights, property or safety.
                    However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>
            </section>
            <section class="mt-4">
                <h6 class="privacy-policy-subtitle">Third-party links</h6>
                <p class="privacy-policy-content">Occasionally, at our discretion, we may include or offer third-party products or services on our website. These third-party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.</p>
            </section>
            <section class="mt-4">
                <h6 class="privacy-policy-subtitle">Google</h6>
                <p class="privacy-policy-content">Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users. Reference link. We use Google AdSense Advertising on our website.</p>
                <p class="privacy-policy-content">Google, as a third-party vendor, uses cookies to serve ads on our site. Google's use of the DART cookie enables it to serve ads to our users based on previous visits to our site and other sites on the Internet. Users may opt-out of the use of the DART cookie by visiting the Google Ad and Content Network privacy policy.
                    We have implemented the following:</p>
                <ul class="py-0 px-3">
                    <li>Demographics and Interests Reporting</li>
                </ul>
                <p class="privacy-policy-content">We, along with third-party vendors such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user interactions with ad impressions and other ad service functions as they relate to our websit</p>
            </section>
            <section class="mt-4">
                <h6 class="privacy-policy-subtitle">Opting out:</h6>
                <p class="privacy-policy-content">Users can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, you can opt out by visiting the Network Advertising Initiative Opt Out page or by using the Google Analytics Opt Out Browser add on.</p>
            </section>
            <section class="mt-4">
                <h6 class="privacy-policy-subtitle">
                    <strong>How does our site handle Do Not Track signals?</strong>
                </h6>
                <p class="privacy-policy-content">We honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place.</p>
            </section>
            <section class="mt-4">
                <h6 class="privacy-policy-subtitle">
                    <strong>CAN SPAM Act</strong>
                </h6>
                <p class="privacy-policy-content">The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.
                    We collect your email address in order to:</p>
                <ul class="py-0 px-3">
                    <li>Send information, respond to inquiries, and/or other requests or questions.</li>
                    <li>Process orders and to send information and updates pertaining to orders.</li>
                    <li>Send you additional information related to your product and/or service.</li>
                    <li>Market to our mailing list or continue to send emails to our clients after the original transaction has occurred.</li>
                </ul>
                <p class="other">To be in accordance with CANSPAM, we agree to the following:</p>
                <ul class="py-0 px-3">
                    <li>Not use false or misleading subjects or email addresses.</li>
                    <li>Identify the message as an advertisement in some reasonable way.</li>
                    <li>Include the physical address of our business or site headquarters.</li>
                    <li>Monitor third-party email marketing services for compliance, if one is used.</li>
                    <li>Honor opt-out/unsubscribe requests quickly.</li>
                    <li>Allow users to unsubscribe by using the link at the bottom of each email.</li>
                </ul>
                <p class="other">If at any time you would like to unsubscribe from receiving future emails, you can email us at <span>froyaplaystore@gmail.com</span> and we will promptly remove you from ALL correspondence.</p>
            </section>
        </div>
    </article>
</main>
@endsection
