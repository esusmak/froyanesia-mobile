@extends('m-froyanesia.layouts.master')
@section('page')
    Detail
@endsection
@section('content')
    <header id="other">
        <nav class="navbar navbar-expand">
            <div class="container px-3 py-2">
                <a href="/transaksi" class="title-page"><i class="fas fa-chevron-left"></i></a>
                <h6 class="title-page mx-auto">Detail</h6>
            </div>
        </nav>
    </header>
    <main>
        <article id="detailTransaksi" class="pending p-3">
            <div class="container">
                <section id="header" class="d-flex justify-content-between py-3">
                    <div class="status">
                        <h4 class="title-transaksi-detail">PENDING</h4>
                        @if ($transaction->paymentmethod->code == 'BANK')
                            <p class="subtitle-transaksi-detail">(Menunggu Pembayaran)</p>
                        @elseif ($transaction->paymentmethod->code == "COD")
                            <p class="subtitle-transaksi-detail">(Dibuat)</p>
                        @endif
                    </div>
                </section>
                <section id="detail-bill" class="py-3">
                    <div class="d-flex justify-content-between">
                        <div class="left-section">
                            <p class="content-transaksi-detail mb-3">Kode pesanan :</p>
                            <p class="content-transaksi-detail mb-3">Tanggal memesan :</p>
                        </div>
                        <div class="right-section">
                            <p class="content-transaksi-detail mb-3">{{ $transaction->invoiceCode }}</p>
                            <p class="content-transaksi-detail mb-3">
                                {{ Carbon\Carbon::parse($transaction->dateCreated)->format('d-m-Y H:i:s') }}</p>
                        </div>
                    </div>
                    <p class="content-transaksi-detail mb-3">Item :</p>
                    <div class="d-flex justify-content-between mb-3">
                        <div class="left-section">
                            @foreach ($transaction->transactiondetails as $item)

                                <p class="content-transaksi-detail mb-3">{{ $item->qty }}
                                    {{ $item->variant->product->name }}
                                    {{ $item->pieces ? " ({$item->pieces})" : '(utuh)' }}
                                </p>
                            @endforeach
                        </div>
                        <div class="right-section with-line">
                            @foreach ($transaction->transactiondetails as $t)
                                <p class="content-transaksi-detail mb-3">{{ number_format($t->subtotal) }}</p>
                            @endforeach
                        </div>
                    </div>
                    <div class="d-flex justify-content-between mb-3">
                        <div class="left-section">
                            <p class="content-transaksi-detail">Subtotal :</p>
                        </div>
                        <div class="right-section">
                            <p class="content-transaksi-detail">Rp  {{ number_format($transaction->totalAmount) }}
                            </p>
                        </div>
                    </div>
                    @if(isset($transaction->infodiscount))
                        <p class="content-transaksi-detail mb-1">Diskon atau Free :</p>
                        @foreach($transaction->infodiscount as $item)
                        <div class="d-flex justify-content-between mb-3">
                            <div class="left-section">
                                <p class="content-transaksi-detail">{{$item->description ."({$item->product_name})"}} :</p>
                            </div>
                            <div class="right-section">
                                <p class="content-transaksi-detail">@if($item->discount_rule_id == 1 || $item->discount_rule_id == 2)
                                    Rp  -{{number_format($item->discountCalculate)}}
                                @else
                                    Rp  -{{number_format($item->discount_ldr)}}
                                @endif</p>
                            </div>
                        </div>
                        @endforeach
                    @endif
                    <div class="d-flex justify-content-between mb-3">
                        <div class="left-section">
                            @if ($transaction->deliveryCost != 0)
                                <p class="content-transaksi-detail">Ongkir :</p>
                            @endif
                        </div>
                        <div class="right-section with-line">
                            @if ($transaction->deliveryCost != 0)
                                <p class="content-transaksi-detail">Rp
                                    {{ number_format($transaction->deliveryCost) }}
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="d-flex justify-content-between">
                        <div class="left-section">
                            <p class="content-transaksi-detail">Total :</p>
                        </div>
                        <div class="right-section">
                            <p class="content-transaksi-detail">Rp  {{ number_format($transaction->totalBuying) }}</p>
                        </div>
                    </div>
                </section>
                <div class="d-flex justify-content-between mb-1">
                    <div class="left-section">
                        <p class="content-transaksi-detail mb-1">Metode Pembayaran :</p>
                    </div>
                    <div class="right-section">
                        <p class="content-transaksi-detail mb-1">{{ $transaction->paymentMethod }}</p>
                    </div>
                </div>
                @if ($transaction->paymentMethod == 'BANK')
                    <div class="d-flex justify-content-between">
                            <div class="card p-4 mb-1">
                            {{ $transaction->bank->bank_code }} a/n : {{ $transaction->bank->person_name }} norek : {{ $transaction->bank->no_rek }}
                            </div>
                            <figure class="d-flex mb-0">
                                <img src="{{asset('assets/image/Icons/Copy.svg')}}" onclick="copyToClipboard('{{ $transaction->bank->no_rek }}')" alt="Copy to Clipboard" class="img-copy mt-auto align-self-end">
                            </figure>
                    </div>
                @endif
                <div class="d-flex justify-content-between mb-2">
                </div>
                <div class="d-flex justify-content-between mb-3">
                    <div class="left-section">
                        <p class="content-transaksi-detail mb-3">Metode Pengiriman :</p>
                        {{-- <p class="content-transaksi-detail mb-3">Jenis Pengiriman :</p> --}}
                        <p class="content-transaksi-detail mb-3">Waktu Pengiriman :</p>
                        <p class="content-transaksi-detail">Kirim Pesanan ke :</p>
                    </div>
                    <div class="right-section">
                        <p class="content-transaksi-detail mb-3">
                            @if ($transaction->typeOrder == 'delivery')
                                Antar {{$transaction->deliveryType}}
                            @elseif($transaction->typeOrder == 'pickup')
                                Ambil di outlet
                            @endif
                        </p>
                        <p class="content-transaksi-detail mb-3">
                            {{ isset($transaction->deliveryDate) ? Carbon\Carbon::parse($transaction->deliveryDate)->format('d-m-Y H:i:s') : '-'}}
                        </p>
                        <p class="content-transaksi-detail">
                            {{ $transaction->recipientName ?? $transaction->buyer->name }}
                        </p>
                    </div>
                </div>
                <div class="card p-4">
                    {{ $transaction->deliveryAddress ?? $transaction->buyer->address }}
                </div>
{{--
                @if($transaction->tempGift)
                <div class="box-gift mt-4">
                    <h6 class="offer-gift mb-3">Anda berhak mendapatkan {{$transaction->infoGift->productName}} gratis karena telah belanja senilai
                        Rp  {{number_format($transaction->infoGift->buyingPerMonth)}}</h6>
                    <p class="confirm-gift mb-5">Apakah anda ingin mengklaim hadiah ini?</p>
                    <div class="row">
                        <div class="col-6">
                            <button class="btn-confirm-one btn-take-gift" data-status='0'>Tidak</button>
                        </div>
                        <div class="col-6">
                            <button class="btn-confirm btn-take-gift" data-status='1'>Terima</button>
                        </div>
                    </div>
                </div>
                @endif --}}
            </div>
        </article>
    </main>
    @if(!$transaction->tempGift)
    <footer class="p-4">
        <div class="transaksi-action px-2">
            <div class="container">
                <div class="row">
                    @if ($transaction->paymentStatus == 'PENDING' && $transaction->currentStatus == 'PENDING')
                        <div class="col-6 px-2">
                            <a href="#!" class="btn-cancel" data-id="{{ $transaction->id }}">
                                <button class="btn-confirm-one">Batal</button>
                            </a>
                        </div>
                    @endif
                    <div class="col-6 px-2">
                        @if ($transaction->paymentmethod->code == 'BANK' || $transaction->paymentmethod->code == 'COD' || $transaction->paymentmethod->code == 'CASH')
                            <a href="{{ route('transaction.pembayaran', $transaction->id) }}">
                                <button class="btn-confirm">Ubah</button>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
    </footer>
    @endif
@endsection

@push('customJs')
    @if($transaction->tempGift)
    <script>
        $(window).on('load', function() {
            $('#diskonModal').modal('show');
        });
    </script>
    @endif
    <script>
        $('.btn-cancel').on('click', function() {
            const id = $(this).data('id');
            const body = {
                "type": "transactions",
                "attributes": {
                    "status": "CANCEL_BY_USER"
                }
            }
            $.ajax({
                url: `${baseApi}v1/transactions/${id}/changeStatus`,
                headers: {
                    'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
                },
                method: 'PUT',
                data: body,
                success: function(data) {
                    let msg = data.meta
                    Swal.fire({
                        title: `${msg.message}`,
                        icon: 'success',
                        confirmButtonText: "ok"
                    })
                    location.reload()
                },
                error: function(request, textStatus, errorTh) {
                    const res = JSON.parse(request.responseText)
                    if (res.errors) {
                        const msg = res.errors[0].title
                        flashAlert(msg, 'error')
                    }
                }
            })
        })


        function copyToClipboard(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val(element).select();
            document.execCommand("copy");
            $temp.remove();
            Swal.fire({
                icon: 'success',
                title: `nomor rekening berhasil dicopy`,
                width : 400
            })

        }
    </script>
@endpush
