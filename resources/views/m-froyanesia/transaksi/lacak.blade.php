@extends('m-froyanesia.layouts.master')
@section('page')
    Lacak
@endsection
@section('content')
    <header id="other">
        <nav class="navbar navbar-expand">
            <div class="container px-3 py-2">
                <a href="{{url()->previous()}}" class="title-page"><i class="fas fa-chevron-left"></i></a>
                <h6 class="title-page mx-auto">Lacak</h6>
            </div>
        </nav>
    </header>
    <main>
        <article id="lacakTransaksi"  class="paid">
            <div class="container p-3">
                <section id="kurirAccount" class="d-flex justify-content-between py-3">
                    <div class="kurir">
                        <div class="d-flex flex-row">
                            @if(!empty($transaction->transporter))
                            <img src="{{$transaction->transporter->user->photo == null ? asset('assets/image/no-photo.png') : $transaction->transporter->user->photo}}" alt="transporter photo profile" class="photo-kurir">
                            <div class="ml-2 ml-sm-3 pt-1">
                                <p class="name-kurir">{{$transaction->transporter->user->name}}</p>
                                <p class="id-kurir">{{$transaction->transporter->transportation->transportationtype->transType}} - {{$transaction->transporter->transportation->policeNumber}}</p>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="kontak pt-2">
                        <a href="tel:{{$transaction->transporter->phone ?? null}}" class="mr-2">
                            <figure>
                                <img src="{{asset('assets/image/Icons/Call.svg')}}" alt="Telpon Kurir" class="img-kontak">
                            </figure>
                        </a>
                        <a id="whatsapp">
                            <figure>
                                <img src="{{asset('assets/image/Icons/Message.svg')}}" alt="Kirim Pesan ke Kurir" class="img-kontak">
                            </figure>
                        </a>
                    </div>
                </section>
            </div>
            <div id='map'></div>
            <div class="container p-4">
                <h4 class="title-lacak">Diantar ke</h4>
                <div class="card p-4 mb-5">
                    <p>{{$transaction->deliveryAddress}}</p>
                </div>
                <div class="col-8 mx-auto p-0">
                    <div class="informasi-two">
                        <p class="m-0">Sedang menuju rumahmu</p>
                    </div>
                </div>
            </div>
        </article>
    </main>
@endsection

@push('customJs')
<script>
    let phone = `${!! $transaction->transporter->whatsapp ?? null !!}`
    phone = phone.replace(/\D/g,'');
    // phone = phone.replace(/^0+/, '62');
    while(phone.charAt(0) === '0')
    {
        phone = phone.substring(1);
        phone = "62"+phone
    }
    $('#whatsapp').attr('href', `https://wa.me/`+phone)
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_API_KEY')}}&ensory=false"></script>
<script>
  const lat = `{!! $transaction->transporter->trackingtransporter->latitude !!}`
  const long = `{!! $transaction->transporter->trackingtransporter->longitude !!}`
   var myLatlng = new google.maps.LatLng(lat,long);
    var mapOptions = {
    zoom: 14,
    center: myLatlng
    }
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);
    // Place a draggable marker on the map
    var iconBase = "/assets/image/";
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        icon: iconBase + "Marker.png",
    });


</script>
@endpush