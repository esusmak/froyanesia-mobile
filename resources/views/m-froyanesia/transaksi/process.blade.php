@extends('m-froyanesia.layouts.master')
@section('page')
    Detail
@endsection
@section('content')
    <header id="other">
        <nav class="navbar navbar-expand">
            <div class="container px-3 py-2">
                <a href="/transaksi" class="title-page"><i class="fas fa-chevron-left"></i></a>
                <h6 class="title-page mx-auto">Detail</h6>
            </div>
        </nav>
    </header>
    <main>
        <article id="detailTransaksi" class="process p-3">
            <div class="container">
                <section id="header" class="d-flex justify-content-between py-3">
                    <div class="kurir">
                        <div class="d-flex flex-row">
                            @if ($transaction->transporter)
                                <img src="{{ $transaction->transporter->user->photo == null ? asset('assets/image/no-photo.png') : $transaction->transporter->user->photo }}"
                                    alt="transporter photo profile" class="photo-kurir">
                                <div class="ml-2 ml-sm-3 pt-1">
                                    <p class="name-kurir">{{ $transaction->transporter->user->name }}</p>
                                    {{-- <p class="id-kurir">ID Kurir : 662599</p> --}}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="status">
                        <h4 class="title-transaksi-detail">PAID</h4>
                        <p class="subtitle-transaksi-detail">(Proses)</p>
                    </div>
                </section>
                <section id="detail-bill" class="py-3">
                    <div class="d-flex justify-content-between">
                        <div class="left-section">
                            <p class="content-transaksi-detail mb-3">Kode pesanan :</p>
                            <p class="content-transaksi-detail mb-3">Tanggal memesan :</p>
                        </div>
                        <div class="right-section">
                            <p class="content-transaksi-detail mb-3">{{ $transaction->invoiceCode }}</p>
                            <p class="content-transaksi-detail mb-3">
                                {{ Carbon\Carbon::parse($transaction->dateCreated)->format('d-m-Y H:i:s') }}</p>
                        </div>
                    </div>
                    <p class="content-transaksi-detail mb-3">Item :</p>
                    <div class="d-flex justify-content-between mb-3">
                        <div class="left-section">
                            @foreach ($transaction->transactiondetails as $item)

                                <p class="content-transaksi-detail mb-3">{{ $item->qty }}
                                    {{ $item->variant->product->name }}
                                    {{ $item->pieces ? " ({$item->pieces})" : '(utuh)' }}
                                </p>
                            @endforeach
                        </div>
                        <div class="right-section with-line">
                            @foreach ($transaction->transactiondetails as $t)
                                <p class="content-transaksi-detail mb-3">{{ number_format($t->subtotal) }}</p>
                            @endforeach
                        </div>
                    </div>
                    <div class="d-flex justify-content-between mb-3">
                        <div class="left-section">
                            <p class="content-transaksi-detail">Subtotal :</p>
                        </div>
                        <div class="right-section">
                            <p class="content-transaksi-detail">Rp  {{ number_format($transaction->totalAmount) }}
                            </p>
                        </div>
                    </div>
                    @if(isset($transaction->infodiscount))
                        <p class="content-transaksi-detail mb-1">Diskon atau Free :</p>
                        @foreach($transaction->infodiscount as $item)
                        <div class="d-flex justify-content-between mb-3">
                            <div class="left-section">
                                <p class="content-transaksi-detail">{{$item->description ."({$item->product_name})"}} :</p>
                            </div>
                            <div class="right-section">
                                <p class="content-transaksi-detail">@if($item->discount_rule_id == 1 || $item->discount_rule_id == 2)
                                    Rp  -{{number_format($item->discountCalculate)}}
                                @else
                                    Rp  -{{number_format($item->discount_ldr)}}
                                @endif</p>
                            </div>
                        </div>
                        @endforeach
                    @endif
                    <div class="d-flex justify-content-between mb-3">
                        <div class="left-section">
                            @if ($transaction->deliveryCost != 0)
                                <p class="content-transaksi-detail">Ongkir :</p>
                            @endif
                        </div>
                        <div class="right-section with-line">
                            @if ($transaction->deliveryCost != 0)
                                <p class="content-transaksi-detail">Rp 
                                    {{ number_format($transaction->deliveryCost) }}
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="d-flex justify-content-between">
                        <div class="left-section">
                            <p class="content-transaksi-detail">Total :</p>
                        </div>
                        <div class="right-section">
                            <p class="content-transaksi-detail">Rp  {{ number_format($transaction->totalBuying) }}</p>
                        </div>
                    </div>
                </section>
                <div class="d-flex justify-content-between mb-3">
                    <div class="left-section">
                        <p class="content-transaksi-detail mb-3">Metode Pembayaran :</p>
                        <p class="content-transaksi-detail mb-3">Metode Pengiriman :</p>
                        {{-- <p class="content-transaksi-detail mb-3">Jenis Pengiriman :</p> --}}
                        <p class="content-transaksi-detail mb-3">Waktu Pengiriman :</p>
                        <p class="content-transaksi-detail">Kirim Pesanan ke :</p>
                    </div>
                    <div class="right-section">
                        <p class="content-transaksi-detail mb-3">{{ $transaction->paymentMethod }}</p>
                        <p class="content-transaksi-detail mb-3">
                            @if ($transaction->typeOrder == 'delivery')
                                Antar
                            @elseif($transaction->typeOrder == 'pickup')
                                Ambil di outlet
                            @endif
                        </p>
                        <p class="content-transaksi-detail mb-3">
                            {{ isset($transaction->deliveryDate) ? Carbon\Carbon::parse($transaction->deliveryDate)->format('d-m-Y H:i:s') : '-'}}
                        </p>
                        <p class="content-transaksi-detail">
                            {{ $transaction->recipientName ?? $transaction->buyer->name }}
                        </p>
                    </div>
                </div>
                <div class="card p-4">
                    {{ $transaction->deliveryAddress ?? $transaction->buyer->address }}
                </div>
                
                {{-- @if($transaction->tempGift)
                <div class="box-gift mt-4">
                    <h6 class="offer-gift mb-3">Anda berhak mendapatkan {{$transaction->infoGift->productName}} gratis karena telah belanja senilai
                        Rp  {{number_format($transaction->infoGift->buyingPerMonth)}}</h6>
                    <p class="confirm-gift mb-5">Apakah anda ingin mengklaim hadiah ini?</p>
                    <div class="row">
                        <div class="col-6">
                            <button class="btn-confirm-one btn-take-gift" data-status='0'>Tidak</button>
                        </div>
                        <div class="col-6">
                            <button class="btn-confirm btn-take-gift" data-status='1'>Terima</button>
                        </div>
                    </div>
                </div>
                @endif --}}
            </div>
        </article>
    </main>
    @if ($transaction->paymentMethod != 'CASH' || $transaction->paymentMethod != 'TUNAI')
        <footer class="p-4">
            <div class="transaksi-action px-2">
                <div class="container">
                    <div class="row">
                        <div class="col-6 p-0 pr-2">
                            <a href="{{ route('transaction.lacak', $transaction->id) }}">
                                <button class="btn-confirm">Lacak</button>
                            </a>
                        </div>
                        <div class="col-6 p-0 pl-2">
                            <a href="#!">
                                <button class="btn-confirm btn-confirmed">Konfirmasi</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    @endif
@endsection

@push('customJs')
    <script>
        $('.btn-confirmed').on('click', function() {
            const body = {
                "type": "transactions",
                "attributes": {
                    "status": "FINISHED"
                }
            }

            $.ajax({
                url: `${baseApi}v1/transactions/{!! $transaction->id !!}/changeStatus`,
                method: 'PUT',
                data: body,
                headers: {
                    'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
                },
                success: function(data) {
                    let res = data
                    Swal.fire({
                        title: res.meta.message,
                        icon: 'info',
                        confirmButtonText: 'Ok'
                    })
                    let redirect = () => {
                        location.reload()
                    }
                    setTimeout(redirect, 1000)
                },
                error: function(request, textStatus, errorThrown) {
                    let msg = JSON.parse(request.responseText)
                    Swal.fire({
                        title: `${msg.errors[0].title}`,
                        icon: 'error',
                        confirmButtonText: "ok"
                    })
                    let redirect = () => {
                        location.reload()
                    }
                    setTimeout(redirect, 1000)
                }
            })
        })

    </script>
@endpush
