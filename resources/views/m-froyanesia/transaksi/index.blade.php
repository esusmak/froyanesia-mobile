@extends('m-froyanesia.layouts.master')
@section('page')
    Transaksi
@endsection
@section('content')
    <header id="other">
        <nav class="navbar navbar-expand">
            <div class="container px-3 py-2">
                <a href="/" class="title-page"><i class="fas fa-chevron-left"></i></a>
                <h6 class="title-page mx-auto">Transaksi</h6>
            </div>
        </nav>
    </header>
    <main>
        {{-- <div class="col-11 mx-auto mt-4 mb-4 px-3">
            <form action="" class="form-search">
                <div class="card">
                    <div class="row">
                        <div class="col-9 col-sm-10 pr-0">
                            <input type="text" placeholder="Search">
                        </div>
                        <div class="col-3 col-sm-2 text-center">
                            <img src="{{asset('assets/image/Icons/Search.svg')}}" alt="" class="img-search pt-2 pr-2 pr-sm-0">
                        </div>
                    </div>
                </div>
            </form>
        </div> --}}
        @include('m-froyanesia.transaksi.filter')
        @include('m-froyanesia.transaksi.transaksi-list')
    </main>
@endsection