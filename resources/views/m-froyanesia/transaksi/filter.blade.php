<article id="filter-container" class="py-1">
    <div id="filter-transaksi" class="py-1 pl-2 pr-3">
        <div>
            <div class="card {{Request::get('status') == null ? 'active' : '' }} border-0 px-4 py-1">
                <a href="/transaksi">All</a>
            </div>
        </div>
        <div>
            <div class="card {{Request::get('status') == 'done' ? 'active' : '' }} border-0 px-4 py-1">
                <a href="/transaksi?status=done">Done</a>
            </div>
        </div>
        <div>
            <div class="card {{Request::get('status') == 'pending' ? 'active' : '' }} border-0 px-4 py-1">
                <a href="/transaksi?status=pending">Pending</a>
            </div>
        </div>
        <div>
            <div class="card {{Request::get('status') == 'canceled' ? 'active' : '' }} border-0 px-4 py-1">
                <a href="/transaksi?status=canceled">Canceled</a>
            </div>
        </div>
        <div>
            <div class="card {{Request::get('status') == 'paid' ? 'active' : '' }} border-0 px-4 py-1">
                <a href="/transaksi?status=paid">Paid</a>
            </div>
        </div>
        <div>
            <div class="card {{Request::get('status') == 'delivering' ? 'active' : '' }} border-0 px-4 py-1">
                <a href="/transaksi?status=delivering">Delivering</a>
            </div>
        </div>
    </div>
</article>