<article id="transaksi-list" class="p-3 mt-4">
    <div class="container" id="transaksiAll">
        @foreach ($transactions->getData() as $t)
        @php
            if ($t->currentStatus == 'PENDING' && $t->paymentmethod->code == "BANK") {
                $currentStatus = 'PENDING';
                $desc = 'Menunggu Pembayaran';
                $class = 'pending';
            }
            else if ($t->currentStatus == 'PENDING' && ($t->paymentmethod->code == "COD" || $t->paymentmethod->code == "CASH")) {
                $currentStatus = 'PENDING';
                $desc = 'Dibuat';
                $class = 'pending';
            }
             elseif ($t->currentStatus == 'PAID') {
                $currentStatus = 'PAID';
                $desc = 'Lunas';
                $class = 'paid';
            } elseif ($t->currentStatus == 'FINISHED') {
                $currentStatus = 'DONE';
                $desc = 'Selesai';
                $class = 'done';
            } elseif ($t->currentStatus == 'DELIVERING' || $t->currentStatus == 'PROCESSED') {
                $currentStatus = 'PAID';
                $desc = 'Proses';
                $class = 'paid';
            } else {
                $currentStatus = 'CANCELED';
                $desc = 'Dibatalkan';
                $class = 'canceled';
            }
            
        @endphp
        <a href="{{route('transaction.show', $t->id)}}">
            <div class="card {{$class}} p-3 mb-3">
                <div class="d-flex justify-content-between">
                    <section class="status">
                        <h4 class="title-transaksi">{{$currentStatus}}</h4>
                        <p class="content-transaksi">({{$desc}})</p>
                        <p class="content-transaksi">{{$t->payerEmail}}</p>
                    </section>
                    <section class="detail text-right">
                        <p class="content-transaksi">Total Belanja : Rp {{number_format($t->totalBuying)}}</p>
                        <p class="content-transaksi">Tanggal Belanja : {{ Carbon\Carbon::parse($t->dateCreated)->format('d-m-Y')}}</p>
                        <p class="content-transaksi">Kode Belanja : {{$t->invoiceCode}}</p>
                    </section>
                </div>
            </div>
        </a>
        @endforeach
    </div>
    <div class="row">
        @if (isset($transactions->getLinks()->next))
        <div class="col-md-12 text-center">
            <button class="see-more btn btn-sm btn-primary" data-link="{{$transactions->getLinks()->next->getHref()}}" data-div="#transaksiAll">Muat lebih banyak</button> 
        </div>
        @endif
    </div>
</article>

@push('customJs')
<script>
    function format_rupiah(angka, prefix = "Rp"){
			var number_string = angka.toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? ',' : '';
				rupiah += separator + ribuan.join(',');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
	}


    $(".see-more").on('click', function() {
            $div = $($(this).data('div')); //div to append
            $link = $(this).data('link'); //current URL
            $href = $link + '&include=productimages'; //complete URL
            $html = ''
            $.ajax({
                url: $href,
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + {!! json_encode(session()->get('token'))!!}
                },
                success:function(data) {
                    res = data
                    res.data.forEach((value, key) => {
                        const id = value.id
                        const currentStatus = value.attributes.currentStatus
                        const payerEmail = value.attributes.payerEmail
                        const totalBuying = value.attributes.totalBuying
                        let dateCreated = new Date(value.attributes.dateCreated)
                        dateCreated = `${dateCreated.getDate()}-${dateCreated.getMonth()}-${dateCreated.getFullYear()}`
                        const invoiceCode = value.attributes.invoiceCode

                        let resultStatus = [];
                        let dataStatus = [
                                    [   
                                        "PENDING",
                                        "PENDING",
                                        "Menunggu pembayaran",
                                        "pending"
                                    ],
                                    [   
                                        "PENDING",
                                        "PENDING",
                                        "Dibuat",
                                        "pending"
                                    ],
                                    [
                                        "PAID",
                                        "PAID",
                                        "Lunas",
                                        "paid"
                                    ],
                                    [
                                        "FINISHED",
                                        "DONE",
                                        "Selesai",
                                        "done"
                                    ],
                                    [
                                        "DELIVERING",
                                        "PAID",
                                        "Proses",
                                        "paid"
                                    ],
                                    [
                                        "PROCESSED",
                                        "PAID",
                                        "Proses",
                                        "paid"
                                    ],
                                    [
                                        "CANCEL_BY_SYSTEM",
                                        "CANCELED",
                                        "Dibatalkan",
                                        "canceled"
                                    ],
                                    [
                                        "CANCEL_BY_ADMIN",
                                        "CANCELED",
                                        "Dibatalkan",
                                        "canceled"
                                    ],
                            ];
                        
                        for (let index = 0; index < dataStatus.length; index++) {
                            if(currentStatus == dataStatus[index][0]) {
                                resultStatus = dataStatus[index]
                            }
                        }
                        
                        $html += `<a href="/transaksi/${id}">
                                <div class="card ${resultStatus[3]} p-3 mb-3">
                                    <div class="d-flex justify-content-between">
                                        <section class="status">
                                            <h4 class="title-transaksi">${resultStatus[1]}</h4>
                                            <p class="content-transaksi">(${resultStatus[2]})</p>
                                            <p class="content-transaksi">${payerEmail}</p>
                                        </section>
                                        <section class="detail text-right">
                                            <p class="content-transaksi">Total Belanja : ${totalBuying}</p>
                                            <p class="content-transaksi">Tanggal Belanja : ${dateCreated}</p>
                                            <p class="content-transaksi">Kode Belanja : ${invoiceCode}</p>
                                        </section>
                                    </div>
                                </div>
                            </a>`

                    })
                    
                    $div.append($html);

                    if(!res.links.next) {
                        $('.see-more').remove()
                        return false
                    }
                    let nextUrl = res.links.next;

                    $('.see-more').data('link', nextUrl)
                }
            });
        });
</script>
@endpush