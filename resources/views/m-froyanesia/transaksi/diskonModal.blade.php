<div class="modal fade" id="diskonModal" data-keyboard="false" tabindex="-1" aria-labelledby="diskonModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mx-auto">
        <div class="modal-content">
            <div class="modal-body text-center ">
                <h6 class="mb-3">Yeay, anda berhak mendapatkan {{$calculate->data->attributes->infoGift->productName}}  karena total belanja bulanan anda senilai Rp {{number_format($calculate->data->attributes->infoGift->buyingPerMonth)}}
                </h6>
                <p class="mb-5">Apakah anda ingin mengklaim hadiah tersebut ?</p>
            </div>
            <div class="modal-footer">
                <div class="w-100">
                    <div class="row">
                        <div class="col-6">
                            <button class="btn-cancel btn-claim" data-status="0" data-dismiss="modal" aria-label="Close">Tidak</button>
                        </div>
                        <div class="col-6">
                            <button class="btn-choose btn-claim" data-status="1" data-dismiss="modal">Klaim</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('customJs')
<script>
    $('#diskonModal').on('click', '.btn-take-gift', function() {
        const status = $(this).data('status');
        const body = {
            "type": "transactions",
            "attributes": {
                "take": status
            }
        }
        $.ajax({
            url: `${baseApi}v1/transactions/{{$transaction->id}}/takegift`,
            headers: {
                'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
            },
            method: 'PUT',
            data: body,
            success: function(data) {
                let msg = data.meta
                Swal.fire({
                    title: `${msg.message}`,
                    icon: 'success',
                    confirmButtonText: "ok"
                })
                location.reload()
            },
            error: function(request, textStatus, errorTh) {
                const res = JSON.parse(request.responseText)
                if (res.errors) {
                    const msg = res.errors[0].title
                    flashAlert(msg, 'error')
                }
            }
        })
    })
</script>
@endpush
