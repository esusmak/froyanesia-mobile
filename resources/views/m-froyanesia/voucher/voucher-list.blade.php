<div class="modal fade" id="voucher" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable mx-auto">
        <div class="modal-content">
            <div class="modal-body">
                <ul class="voucher-list">
                    @forelse($vouchers as $item)
                    <li class="d-flex justify-content-between">
                        <button class="voucher-data" 
                        data-voucher_code="{{$item->code}}" 
                        data-title="{{$item->data->title}}"
                        data-discount_type="{{$item->data->discount_type}}"
                        data-discount_value="{{$item->data->discount_value}}"
                        data-voucher_id="{{$item->id}}">
                            <div class="card p-2">
                                <div class="d-flex flex-row">
                                    <img src="{{asset('assets/image/Icons/Voucher.svg')}}" alt="Voucher" class="img-fluid">
                                    <span class="name-voucher mr-auto pl-3">{{$item->data->title}}</span>
                                </div>
                            </div>
                        </button>
                        <a href="{{route('voucher.show', $item->id)}}" class="to-detail"><i class="fas fa-chevron-right"></i></a>
                    </li>
                    @empty
                    <li class="d-flex justify-content-between">
                        <span class="mr-auto pl-3">Tidak ada voucher</span>
                    </li>
                    @endforelse
                    {{-- <li class="d-flex justify-content-between">
                        <button class="voucher-data expired">
                            <div class="card p-2">
                                <div class="d-flex flex-row">
                                    <img src="{{asset('assets/image/Icons/Voucher.svg')}}" alt="Voucher" class="img-fluid">
                                    <span class="mr-auto pl-3">Ayam Ceker 10%</span>
                                </div>
                            </div>
                        </button>
                        <a href="/voucher" class="to-detail"><i class="fas fa-chevron-right"></i></a>
                    </li> --}}
                </ul>
            </div>
            <div class="modal-footer">
                <div class="w-100">
                    <div class="row">
                        <div class="col-6">
                            <button class="btn-cancel" data-dismiss="modal" aria-label="Close">Batal</button>
                        </div>
                        <div class="col-6" id="applyVoucher">
                            <button class="btn-choose" disabled>Pakai</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('customJs')
<script>
    $(".voucher-list").on("click", '.voucher-data',function() {
        const voucherCode = $(this).data('voucher_code');
        const voucherId = $(this).data('voucher_id')
        const discount_type = $(this).data('discount_type')
        const discount_value = $(this).data('discount_value')
        const title = $(this).data('title')
        $(".btn-choose").attr('disabled', false)
        $(".btn-choose").data('voucher_code', voucherCode)
        $(".btn-choose").data('voucher_id', voucherId)
        $('.btn-choose').data('discount_type', discount_type)
        $('.btn-choose').data('discount_value', discount_value)
        $(".btn-choose").data('title', title)
    })

    $('#applyVoucher').on('click', '.btn-choose', function() {
        const voucherCode = $(this).data('voucher_code')
        const voucherId = $(this).data('voucher_id')
        const title = $(this).data('title')
        const discount_value = $(this).data('discount_value')
        const discount_type = $(this).data('discount_type')

        const body = {
            "type": "transactions",
            "attributes": {
                "voucherId": voucherId,
            }
        }
        $.ajax({
            url: `${baseApi}v1/transactions/calculate`,
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + {!! json_encode(session()->get('token'))!!}
            },
            data: body,
            success: function( data ) {
                let res = data.data
                
                Swal.fire({
                    title: `${res.attributes.info}`,
                    icon: 'info',
                    confirmButtonText: "ok"
                })

                if(res.attributes.discount != 0) {
                    let discountText = `- Rp  ${discount_value}`
                    if(discount_type == 'percent') {
                        discountText = `${discount_value}%`
                    }

                    localStorage.setItem('voucherId', voucherId)
                    $('#voucherApplied').empty()
                    $('#voucherApplied').text(voucherCode)
                    
                    let html = `<p class="data-voucher">${title}</p>
                                <p class="data-voucher">${discountText}</p>`
                    $('.applied').html(html)

                    $('.btn-voucher').addClass('d-none')
                    $('#voucher').modal('hide')
                }
            },
            error: function(request, textStatus, errorThrown) {
                let msg = JSON.parse(request.responseText)
                Swal.fire({
                    title: `${msg.errors[0].title}`,
                    icon: 'error',
                    confirmButtonText: "ok"
                })
            }
        })
    });
</script>
@endpush