@extends('m-froyanesia.layouts.master')
@section('page')
    Voucher
@endsection
@section('content')
    <header id="other">
        <nav class="navbar navbar-expand">
            <div class="container px-3 py-2">
                <a href="/keranjang" class="title-page"><i class="fas fa-chevron-left"></i></a>
                <h6 class="title-page mx-auto">Voucher</h6>
            </div>
        </nav>
    </header>
    <main>
        @if(isset($voucher->data->image))
        <figure class="m-0 p-0">
            <img src="{{$voucher->data->image}}" alt="Gambar Voucher" class="w-100">
        </figure>
        @endif
        <h3 class="title-voucher text-center pb-4">{{$voucher->data->title}}</h3>
        <div class="voucher-detail py-3 px-4">
            <div class="container">
                <p>{{$voucher->data->description}}</p>
                <p class="mb-2 mt-4">Diskon berlaku hingga tanggal</p>
                <p class="date">{{date('d F Y', strtotime($voucher->expiresAt))}}</p>
            </div>
        </div>
    </main>
@endsection