<section id="product" class="mt-4">
    <div class="row" id="product-all">
        @foreach($products->getData() as $item)
        <div class="col-6 mb-4">

            <div class="card {{$item->cheapestStock == 0 ? "stock-none" : ''}}">
            
                <div class="top-card" style="background: url({{!empty($item->productimages) ? $item->productimages[0]->file : ''}}) no-repeat center; background-size: cover;">
                    {{-- <div class="overlay"></div> --}}
                    @if($item->isDiscount)
                    <div class="price">
                        <div class="col-7 p-1">
                            <p class="item-price text-coret">Rp {{number_format($item->priceBeforeDiscount)}}</p>

                            <p class="item-diskon mt-1">Rp {{number_format($item->cheapestPrice)}}</p>
                           
                        </div>
                        <div class="col-2 p-0">
                                <p class="diskon-persen mt-1">{{$item->savePrice}}</p>
                        </div>
                        
                    </div>
                    @else
                    <div class="price">
                        <div class="col-7 p-0">
                            <p class="item-price">Rp {{number_format($item->cheapestPrice)}}</p>
                        </div>
                    </div>
                    @endif
                    <a href="{{$item->cheapestStock == 0 ? "stock-none" : route('product.show', $item->id)}}">
                    <h6 class="item-name">{{$item->name}}</h6>
                    </a>
                </div>
                <div class="bot-card">
                    <a href="{{$item->cheapestStock == 0 ? "#!" : route('product.show', $item->id)}}">
                        <button class="btn-add">Lihat</button>
                    </a>
                </div>
            </div>

        </div>
        @endforeach
    </div>
    <div class="row">
        @if (isset($products->getLinks()->next))
        <div class="col-md-12 text-center">
            <button class="see-more btn btn-sm btn-primary" data-link="{{$products->getLinks()->next->getHref()}}" data-div="#product-all">Muat lebih banyak</button>
        </div>
        @endif
    </div>
</section>

@push('customJs')
<script>

    // $('#product-all').on('click', '.btn-add', function() {
    //     const variant_id = $(this).data('variant_id')
    //     const qty = 1;
    //     let body = {
    //         "type": "carts",
    //         "attributes": {
    //             "variantId": variant_id,
    //             "qty": 1
    //         }
    //     }
   
    //     $.ajax({
    //         url: `${baseApi}v1/carts`,
    //         headers: {
    //             'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
    //         },
    //         method: 'POST', 
    //         data: body,
    //         success: function( data ) {
    //             qtyParent.val(qty)
    //             let msg = data.meta
    //             Swal.fire({
    //                 title: `${msg.message}`,
    //                 icon: 'success',
    //                 confirmButtonText: "ok"
    //             })
    //             // location.reload()
    //         }, error: function(request, textStatus, errorTh) {
    //             const res = JSON.parse(request.responseText)
    //             if(res.errors) {
    //                 const msg = res.errors[0].title
    //                 flashAlert(msg, 'error')
    //             }
    //         }
    //     })
    // })

    $(".see-more").on('click', function() {
        $div = $($(this).data('div')); //div to append
        $link = $(this).data('link'); //current URL
        $href = $link + '&include=productimages'; //complete URL
        $html = ''
        $.get($href, function(response) { //append data
            res = response
            console.log(res)
            res.data.forEach((value, key) => {
                id = value.id
                name = value.attributes.name
                description = value.attributes.description
                cheapestPrice = value.attributes.cheapestPrice
                cheapestVariantId = value.attributes.cheapestVariantId

                let include = res.included

                let resultInclude = []
                include.forEach((value, key) => {
                    if (value.attributes.productId == id) {
                        resultInclude = {
                            "type": value.type,
                            "attributes": value.attributes
                        }
                    }
                })

                $html += `<div class="col-6 mb-4">
                        <div class="card px-3 pt-3">
                            <a href="/products/${id}">
                                <figure class="p-0 text-center">
                                    <img src="${resultInclude.attributes.file}" alt="${name}" class="img-fluid rounded">
                                </figure>
                                <h6 class="item-name">${name}</h6>
                                <p class="item-intro mt-1">${description}</p>
                                <p class="item-price">Rp ${cheapestPrice}</p>
                            </a>
                            <button class="btn-add" data-variant_id="${cheapestVariantId}">+</button>
                        </div>
                    </div>`

            })

            $div.append($html);

            if (!res.links.next) {
                $('.see-more').remove()
                return false
            }
            let nextUrl = res.links.next;

            $('.see-more').data('link', nextUrl)
        });
    });
</script>
@endpush