@extends('m-froyanesia.layouts.master')
@section('page')
    Beranda
@endsection
@section('content')
    @include('m-froyanesia.partials.preloader')
    <header class="primary">
        <nav class="navbar home">
            <div class="container p-2">
                <div class="d-flex justify-content-between w-100">
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon p-0">
                            <img src="{{asset('assets/image/Icons/Main Menu.svg')}}" alt="Menu" class="img-dashboard">
                        </span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img src="{{asset('assets/image/Logo/Froya.svg')}}" alt="Froyanesia" class="img-logo">
                    </a>
                    <a href="/notifikasi" class="notif">
                        <img src="{{asset('assets/image/Icons/Notifikasi.svg')}}" alt="Notifikasi" class="img-notif">
                        <span></span>
                    </a>
                </div>
            </div>
            @include('m-froyanesia.partials.nav-beranda')
        </nav>
    </header>
    <main>
        <article id="dashboard" class="px-4 py-2">
            <div class="container">
                <div class="col-12 mt-5 mb-4 p-0">
                    <form action="{{route('public.home')}}" class="form-search">
                        {{-- <div class="card">
                            <div class="row">
                                <form action="{{route('public.home')}}">
                                    <div class="col-9 col-sm-10 pr-0">
                                        <input type="text" name="search" placeholder="Search">
                                    </div>
                                    <div class="col-3 col-sm-2 text-center">
                                        <img src="{{asset('assets/image/Icons/Search.svg')}}" alt="" class="img-search pt-2 pr-2 pr-sm-0">
                                    </div>
                                </form>
                            </div>
                        </div> --}}
                        <div class="card">
                                <input type="text" name="search" placeholder="Cari">
                                <img src="{{asset('assets/image/Icons/Search.svg')}}" alt="Search" class="img-search">
                        </div>
                    </form>
                </div>
                @include('m-froyanesia.dashboard.kategori')
                @include('m-froyanesia.dashboard.produk')
            </div>
        </article>
    </main>
    <footer>
        @include('m-froyanesia.partials.bot-nav')
    </footer>
    @include('m-froyanesia.partials.add-cart-success')
@endsection

