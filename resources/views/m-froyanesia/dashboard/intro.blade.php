@extends('m-froyanesia.layouts.master')
@section('page')
    Intro Page
@endsection
@section('content')
<main id="single">
    <div class="intro-inner">
        @foreach($introSlider->getData() as $key => $item)
            @if($key != 0)
                <img src="{{$item->file}}" alt="Froyanesia" id="img-{{$loop->iteration}}" style="display: none">
            @else 
                <img src="{{$item->file}}" alt="Froyanesia" id="img-{{$loop->iteration}}">
            @endif
        @endforeach
    </div>
</main>
<footer class="footer-slider">
    <button class="btn-next">Next</button>
</footer>
{{-- <section class="intro">
    
</section> --}}
@endsection

@push('customJs')
<script>
    const total = `{!! count($introSlider->getData()) !!}`
    console.log(total)
    var index = 1;
    $('.btn-next').on('click', function() {
        // console.log("haloo "+index)
        if(index < total) {
            ++index
            $('#img-'+(index-1)).hide()
            $('#img-'+index).show()
        } else {
            location.reload()
        }
    })
</script>
@endpush