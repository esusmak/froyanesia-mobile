<section id="kategori" class="text-center">
    <div class="kategori-slider">
        @foreach($categoryProduct->getData() as $item)
       <div>
           @php
                $ifClickCategory = Request::get('categoryId') ?? null;
           @endphp
           @if($item->name == 'Semua')
            <div class="card py-2 {{$ifClickCategory == null ? 'semua' : ''}}">
                <a href="{{route('public.home')}}" class="text-center">
                    {{$item->name}}
                </a>
            </div>
            @else
            <div class="card py-2 {{$ifClickCategory == $item->id ? 'semua' : ''}}">
                <a href="{{route('public.home', ['categoryId' => $item->id])}}" class="text-center">
                    <img src="{{$item->logo}}" alt="{{$item->name}}" class="kategori-icons">
                    {{$item->name}}
                </a>
            </div>
            @endif
       </div>
       @endforeach
    </div>
</section>