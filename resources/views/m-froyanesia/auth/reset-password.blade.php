@extends('m-froyanesia.layouts.master')
@section('page')
    Lupa Password
@endsection
@section('content')
    <header id="other">
        <nav class="navbar navbar-expand">
            <div class="container px-3 py-2">
                <a href="/login" class="title-page"><i class="fas fa-chevron-left"></i></a>
            </div>
        </nav>
    </header>
    <main>
        <article id="reset-password" class="p-4">
            <div class="container">
                <h4 class="mb-4">Lupa Password?</h4>
                <p>Masukkan nomor WhatsApp yang Anda gunakan saat bergabung dan kami akan mengirimkan petunjuk untuk menyetel ulang sandi Anda.</p>
                <p>Untuk alasan keamanan, kami TIDAK menyimpan kata sandi Anda. Jadi yakinlah bahwa kami tidak akan pernah mengirimkan kata sandi Anda melalui nomor WhatsApp anda.</p>
                <form action="{{route('auth.forgotPassword')}}" method="post">
                    @csrf
                    <label for="reset" class="custom--label">No. Whatsapp</label>
                    <input type="text" class="input-custom" id="reset" name="emailOrPhone" required>
                    <div class="col-6 col-sm-5 mt-4 p-0">
                        <button type="submit" class="btn-confirm">Kirim</button>
                    </div>
                </form>
            </div>
        </article>
    </main>
@endsection
