@extends('m-froyanesia.layouts.master')
@section('page')
    Daftar
@endsection
@push('customCss')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush
@section('content')
        <header id="other">
            <nav class="navbar navbar-expand">
                <div class="container px-3 py-2">
                    <a href="/login" class="title-page"><i class="fas fa-chevron-left"></i></a>
                    <h6 class="title-page mx-auto">Daftar</h6>
                </div>
            </nav>
        </header>
        <main>
            <article id="register" class="p-3">
                <div class="container">
                    <section id="other-register" class="mb-3">
                            {{-- <div class="col-12">
                                <a href="">
                                    <button class="btn-google"><i class="fa-lg fab fa-google pr-3"></i>Daftar lewat Google</button>
                                </a>
                            </div> --}}
                        {{-- <div class="px-2">
                            <div class="line mt-5"></div>
                            <p class="or">Atau</p>
                        </div> --}}
                    </section>
                    <div class="container mt-4 pt-2">
                        <form action="{{route('public.auth.register')}}" class="form-register" method="post">
                            @csrf
                            <label class="custom--label" for="name">Nama</label>
                            <input type="text" class="input-custom mb-3" id="name" name="name" required>
                            <label class="custom--label" for="email-phone">No. Whatsapp</label>
                            <input type="number" min="0" placeholder="Contoh: 0857xxxx" class="input-custom mb-3" id="email-phone" name="emailOrPhone" required>
                            <label class="custom--label" for="password">Password</label>
                            <div class="input-group-custom mb-3">
                                <input type="password" class="input-custom" id="password" name="password" required>
                                <span toggle="#password" id="toggle-password" class="fas fa-eye"></span>
                            </div>
                            <label class="custom--label" for="address">Alamat</label>
                            <input type="text" class="input-custom mb-3" id="address" required>
                            <input type="hidden" name="villageId" id="villageId">
                            <label class="custom--label" for="addressInfo">Detail Alamat</label>
                            <textarea class="address-container p-3 mb-3 form-control" id="adrressInfo" name="address"></textarea>
                            <input type="checkbox" class="custom-checkbox" id="agree" name="agree">
                            <label class="label-check" for="agree">
                                <span class="box"></span>
                                <span>Membuat akun berarti Anda setuju dengan Persyaratan Layanan, <a href="/privacy-policy" class="privacy">Kebijakan Privasi</a>, dan Pengaturan Notifikasi default kami<span>
                            </label>
                            <div class="col-6 col-sm-5 p-0 mb-5 mt-4">
                                <button type="submit" id="submit-register" class="btn-confirm" disabled>Daftar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </article>
        </main>
        <footer>
            <p class="not-account text-center my-2">Sudah punya akun? <a href="/login">Masuk</a></p>
        </footer>
@endsection

@push('customJs')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
     $( "#address" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: baseApi + `v1/villages?limit=9999&sort=asc&include=district`,
                data: {
                    search: request.term
                },
                success: function( data ) {
                    let res = data.data.map((value, key) => {
                        result = {
                            "name": value.attributes.name,
                            "id": value.id
                        }
                        let included = data.included
                        
                        if(typeof value.relationships !== 'undefined' && typeof included[key] !== 'undefined') {
                            result['district'] = included[key].attributes.name
                        }
                        return result
                    }) 
                    response( res );
                }
            });
        },
        minLength: 3,
        select: function( event, ui ) {
            let villageId = ui.item.id
            let label = ui.item.name +", " + ui.item.district
            $( "#adrressInfo" ).val(label);
            $( "#villageId" ).val(villageId);
            return false;
        },
        open: function() {
            $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function() {
            $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( `<div>${item.name + ', ' + item.district}</div>`)
        .appendTo( ul );
    };
</script>
@endpush