@extends('m-froyanesia.layouts.master')
@section('page')
    Login
@endsection
@section('content')
    <main id="single">
        <article id="login" class="px-3 py-3 px-sm-5">
            <div class="container">
                <div class="login-wrapper">
                    <figure class="text-center">
                        <img src="{{asset('assets/image/Logo/froya holo.png')}}" alt="Froyanesia" class="rounded img-logo">
                    </figure>
                    <form action="{{route('public.auth.loginProcess')}}" method="post" class="login-form mb-2">
                        @csrf
                        <label class="custom-label mb-4">
                            <input type="text" name="emailOrPhone" class="input-login" placeholder=" ">
                            <p>No. Whatsapp</p>
                        </label>
                        <label class="custom-label">
                            <input type="password" class="input-login" placeholder=" " id="pass" name="password">
                            <p>Password</p>
                            <span toggle="#pass" id="toggle-password" class="fas fa-eye"></span>
                        </label>
                        <input type="checkbox" class="custom-checkbox" id="remember" name="remember">
                        <label class="label-check" for="remember">
                            <span class="box"></span>
                            <span>Remember Me<span>
                        </label>
                        <input type="submit" class="btn-login mt-3 mb-2" value="LOG IN">
                    </form>
                    <a href="/reset-password" class="forgot-password">Lupa Password?</a>
                    {{-- <div class="line mt-4"></div> --}}
                    {{-- <p class="or">Atau</p> --}}
                    {{-- <a href="{{route('public.auth.provider', 'google')}}" class="">
                        <button class="btn-google login mt-4"><i class="fa-lg fab fa-google pr-3"></i>Daftar lewat Google</button>
                    </a> --}}
                </div>
            </div>
        </article>
    </main>
    <footer class="login-footer">
        <p class="not-account text-center my-2">Belum Punya Akun Froya? <a href="/register">Daftar</a></p>
    </footer>
@endsection