@extends('m-froyanesia.layouts.master')
@section('page')
    Verifikasi Email
@endsection

@section('content')
    <h5>Verification Email is Loading...</h5>
@endsection

@push('customJs')
<script>
    var token = `{!! app('request')->token !!}`
    $.ajax({
        url: `{!! route('api.public.auth.validateRegister') !!}?token=`+token,
        method: 'GET',
        success: function(data) {
            if(!data.errors) {
                Swal.fire({
                    title: `Akun berhasil di konfirmasi`,
                    icon: 'info',
                    confirmButtonText: 'Ok'
                })
                location.href = '/login'
                return false
            }
            Swal.fire({
                title: `Akun gagal di konfirmasi`,
                icon: 'danger',
                confirmButtonText: 'Ok'
            })
            location.href = '/login'
        }
    })
</script>
@endpush