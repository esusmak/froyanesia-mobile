@extends('m-froyanesia.layouts.master')
@section('page')
Kata Sandi Baru
@endsection
@section('content')
    <header id="other">
        <nav class="navbar navbar-expand">
            <div class="container px-3 py-2">
                <a href="/login" class="title-page"><i class="fas fa-chevron-left"></i></a>
            </div>
        </nav>
    </header>
    <main>
        <article id="passwordBaru" class="p-4">
            <div class="container">
                <h4 class="mb-4">Kata Sandi Baru</h4>
                <p>Silahkan perbarui kata sandimu</p>
                
                <form action="{{route('auth.resetPassword', $token)}}" method="post">
                    @csrf
                    <label for="emailTelp" class="custom--label">No. Whatsapp</label>
                    <input type="text" class="input-custom mb-3" id="emailTelp" name="emailOrPhone" required>
                    <label for="newPassword" class="custom--label">
                        Password
                        <span toggle="#newPassword" id="toggle-password" class="fas fa-eye"></span>
                    </label>
                    <input type="password" class="input-custom" id="newPassword" name="newPassword" required>
                    <div class="col-6 col-sm-5 mt-4 p-0">
                        <button type="submit" class="btn-confirm">Kirim</button>
                    </div>
                </form>
            </div>
        </article>
    </main>
@endsection
