@extends('m-froyanesia.layouts.master')
@section('page')
    Verifikasi
@endsection
@section('content')
    <header id="other">
        <nav class="navbar navbar-expand">
            <div class="container px-3 py-2">
                <a href="/register" class="title-page"><i class="fas fa-chevron-left"></i></a>
                <h6 class="title-page mx-auto">Verifikasi</h6>
            </div>
        </nav>
    </header>
    <main>
        <article id="verification">
            <div class="container text-center">
                <div class="p-4">
                    <figure>
                        <img src="{{asset('assets/image/Icons/Gembok.svg')}}" alt="Verifikasi" class="img-fluid rounded">
                    </figure>
                    <div class="information mt-4">
                        <h5>Masukkan Kode Verifikasi</h5>
                        <p class="mt-3">Kode verifikasi telah dikirim ke whatsapp anda</p>
                        {{-- <p class="numb-email">******326*</p> --}}
                    </div>
                </div>
                <form method="POST" action="{{route('auth.verification')}}" class="digit-group"
                data-group-name="digits"
                data-autosubmit="false"
                autocomplete="off">
                @csrf
                    <p class="mb-4">Kode Verifikasi</p>
                    <input type="text" class="input-code" id="digit-1" name="digit1" data-next="digit-2" />
                    <input type="text" class="input-code" id="digit-2" name="digit2" data-next="digit-3" data-previous="digit-1" />
                    <input type="text" class="input-code" id="digit-3" name="digit3" data-next="digit-4" data-previous="digit-2" />
                    <input type="text" class="input-code" id="digit-4" name="digit4" data-previous="digit-3" />
                    <div class="col-8 mx-auto mt-5">
                        <input type="submit" class="btn-confirm-one verify" value="Verifikasi" disabled>
                    </div>
                </form>
                <p class="resend mt-4">Tidak menerima kode? <a href="#!" id="kirimUlang">Kirim ulang</a></p>
            </div>
        </article>
    </main>
@endsection

@push('customJs')
<script>
    $('#kirimUlang').on('click', function() {
        const phone = `{!! session()->get('phone') !!}`
        const body = {
            "type": "resendCode",
            "attributes": {
                "emailOrPhone": phone
            }
        }
        $.ajax({
            url: `${baseApi}v1/auth/resendcode`,
            method: 'GET',
            data: body,
            success: function( data ) {
                let res = data
                Swal.fire({
                    title: "Berhasil dikirim, silahkan cek kode verifikasi di sms / whatsapp anda",
                    icon: 'info',
                    confirmButtonText: 'Ok'
                })
            },
            error: function(request, textStatus, errorThrown) {
                    Swal.fire({
                        title: 'Tunggu beberapa menit lagi',
                        icon: 'error',
                        confirmButtonText: 'Ok'
                    })
            }
        })
    })
</script>
@endpush
