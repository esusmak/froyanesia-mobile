<ul class="bot-nav text-center">
    <li class="bot-nav-item">
        <a href="/" class="bot-nav-link">
            <img src="{{asset('assets/image/Icons/beranda.svg')}}" alt="Beranda" class="mx-auto">
            <span class="bot-nav-text">Beranda</span>
        </a>
    </li>
    <li class="bot-nav-item">
        <a href="/keranjang" class="bot-nav-link">
            <img src="{{asset('assets/image/Icons/Keranjang.svg')}}" alt="Keranjang" class="mx-auto">
            @if($countCart>0)
            <span class="cart-counter text-white" style="font-size: 11px" id="countCart">{{$countCart}}</span>
            @endif
            <span class="bot-nav-text">Keranjang</span>
        </a>
    </li>
    <li class="bot-nav-item">
        <a href="/profile" class="bot-nav-link">
            <img src="{{asset('assets/image/Icons/Profile.svg')}}" alt="Profile" class="mx-auto">
            <span class="bot-nav-text">Akun</span>
        </a>
    </li>
</ul>