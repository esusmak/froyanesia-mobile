<div class="modal fade" id="successAdd" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mx-auto">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center ">
                <p>Item berhasil ditambahkan di keranjang</p>
                <figure>
                    <img src="{{asset('assets/image/Icons/Done.svg')}}" alt="Success add to cart" class="img-fluid rounded">
                </figure>
            </div>
        </div>
    </div>
</div>