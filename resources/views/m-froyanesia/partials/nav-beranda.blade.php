@php
$user = session()->get('user') ?? null;
@endphp
<div class="collapse navbar-collapse px-3 px-sm-4 pt-5" id="navbar">
   <div class="container pr-5">
       <div class="data-profile py-4">
           <div class="d-flex flex-row align-items-center">
               <img src="{{ !empty($user['attributes']['photo']) ? $user['attributes']['photo'] : asset('assets/image/no-photo.png')}}" alt="" class="photo-profile">
               <div class="ml-3 pt-2">
                   <p class="name-profile">{{ !empty($user) ? $user['attributes']['name'] : 'Belum ada nama'}}</p>
                   <p class="address-profile">{{ !empty($user) ? $user['attributes']['address'] : 'Belum ada alamat'}}</p>
               </div>
           </div>
       </div>
       <ul class="navbar-nav mr-auto py-5">
           <li class="nav-item">
               <a class="nav-link" href="{{route('profile.index')}}">Profil</a>
           </li>
           <li class="nav-item">
               <a class="nav-link" href="/">Dashboard Produk</a>
           </li>
           <li class="nav-item">
               <a class="nav-link" href="{{route('cart.index')}}">Keranjang</a>
           </li>
           <li class="nav-item">
               <a class="nav-link" href="{{route('transaction.index')}}">Transaksi</a>
           </li>

       </ul>
       @if (Session::has('token'))          
       <a href="#!" id="logout" class="log mt-4">Logout</a>
       @else
       <a href="{{route('public.auth.login')}}" class="log mt-4">Login</a>
       @endif
   </div>
</div>

@push('customJs')
<script>
    $("#logout").on('click', function() {
        localStorage.clear()
        location.href = `{!!route('auth.logout')!!}`
    })
</script>
@endpush