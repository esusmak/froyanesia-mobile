@push('customCss')
<style>
    .pac-container {
        z-index: 10000 !important;
    }

</style>
@endpush

<div class="modal fade" id="alamatLain" data-keyboard="false" tabindex="-1" aria-labelledby="alamatLainLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mx-auto">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Alamat</h5>
            </div>
            <div class="modal-body text-center p-0">
                <div class="container">
                    <input type="text" class="input-custom mb-3" placeholder="Nama Penerima" id="recipientName">
                    <select name="provinsi" id="provinsi" class="input-custom mb-3">
                        <option disabled selected>Provinsi</option>
                        @foreach($provinces->getData() as $item)
                            <option value="{{ $item->id }}">{{$item->name}}</option>
                        @endforeach
                    </select>
                    <select  name="kota" id="kota" class="input-custom mb-3">
                        <option disabled selected>Kota</option>
                    </select>
                    <select name="kecamatan" id="kecamatan" class="input-custom mb-3">
                        <option value="">Kecamatan</option>
                    </select>
                    <select name="kecamatan" id="kelurahan" class="input-custom mb-3">
                        <option value="">Kelurahan</option>
                    </select>
                    <input name="alamatModal" id="alamatModal" class="input-custom mb-3" placeholder="Nama Jalan, RT/RW, Gedung, No.Rumah / Unit">
                </div>
                <hr>
                <label for="">Geser marker untuk pilih titik lokasi</label>
                <div class="container">
                    <div class="position-relative">
                        <div class="search-location p-3">
                            <input type="text" id="searchTextField" class="input-custom" placeholder="Cari titik lokasi">
                        </div>
                    </div>
                    <div id="map" style="height: 18rem"></div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="w-100">
                    <div class="row">
                        <div class="col-6">
                            <button class="btn-confirm-one" data-dismiss="modal" aria-label="Close">Batal</button>
                        </div>
                        <div class="col-6">
                            <button class="btn-confirm" data-dismiss="modal" >Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('customJs')
<script>
    // $('#alamatLain').find('#recipientName').keyup(function() {
    //     localStorage.setItem('recipientName', $(this).val())
    // })
    $('#alamatLain').on('click', '.btn-choose',function() {
        const alamatModal = $('#alamatLain').find('#alamatModal').val()
        $('#deliveryAddress').val(alamatModal)
    })

  
    $('#alamatLain').on('change', '#provinsi', function() {
        const provinceId = $(this).val();
        $.ajax({
            url: `{!! route('order.getRegencies') !!}`,
            data: {provinceId: provinceId},
            method: 'GET',
            success: function( data ) {
                const parentRegencies = $('#kota')
                parentRegencies.empty()
                parentRegencies.append(new Option("Pilih kota", ""))
                
                data.data.forEach(item => {
                    parentRegencies.append(new Option(item.attributes.name,item.id))
                });
            }
        })
    })

    $('#alamatLain').on('change', '#kota', function() {
        const regencyId = $(this).val();
        $.ajax({
            url: `{!! route('order.getDistrict') !!}`,
            data: {regencyId: regencyId},
            method: 'GET',
            success: function( data ) {
                const parentDistrict = $('#kecamatan')
                parentDistrict.empty()
                parentDistrict.append(new Option("Pilih Kecamatan", ""))
                
                data.data.forEach(item => {
                    parentDistrict.append(new Option(item.attributes.name,item.id))
                });
            }
        })
    })

    $('#alamatLain').on('change', '#kecamatan', function() {
        const districtId = $(this).val();
        $.ajax({
            url: `{!! route('order.getVillage') !!}`,
            data: {districtId: districtId},
            method: 'GET',
            success: function( data ) {
                const parentDistrict = $('#kelurahan')
                parentDistrict.empty()
                parentDistrict.append(new Option("Pilih kelurahan", ""))
                
                data.data.forEach(item => {
                    parentDistrict.append(new Option(item.attributes.name,item.id))
                });
            }
        })
    })

    $('#alamatLain').on('change', '#kelurahan', function() {
        const villageId = $(this).val();
        // setDeliveryCost(villageId)
        localStorage.setItem('villageId', villageId)
    })

    $('#alamatLain').on('click', '.btn-confirm',function() {

      localStorage.setItem('recipientName', $('#recipientName').val())
      setDeliveryCost($('#kelurahan option:selected').val())

      const address = $('#alamatModal').val()+", "+$('#kelurahan option:selected').text()+", "+$('#kecamatan option:selected').text()+", "+$('#kota option:selected').text()+", "+$('#provinsi option:selected').text()
      $('#deliveryAddress').text(address)
    })
     
    
    

</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_API_KEY')}}&libraries=places"></script>
<script>
//    var myLatlng = new google.maps.LatLng(-7.983908,112.621391);
// var mapOptions = {
//   zoom: 14,
//   center: myLatlng
// }
// var map = new google.maps.Map(document.getElementById("map"), mapOptions);

// // Place a draggable marker on the map
// var marker = new google.maps.Marker({
//     position: myLatlng,
//     map: map,
//     draggable:true,
//     title:"Drag me!"
// });

// google.maps.event.addListener(marker, "dragend", function(){
//     var laturl=marker.getPosition().lat();
//     var lngurl=marker.getPosition().lng();
//     localStorage.setItem('latitude', laturl)
//     localStorage.setItem('longitude', lngurl)			
// });

// const input = document.getElementById("searchTextField");
//     const autocomplete = new google.maps.places.Autocomplete(input);
    
//     autocomplete.addListener('place_changed', function () {
//         var place = autocomplete.getPlace();
        
//         // document.getElementById('city2').value = place.name;
//         // const lat = place.geometry.location.lat();
//         // const long = place.geometry.location.lng();
//         map.setCenter(place.geometry.location);
//         marker.setPosition(place.geometry.location);
//     });


</script>
<script>
  var map, infoWindow;
  var autocompleteSearch = () => {
  var input = document.getElementById("searchTextField");
  var mapOptions = {
    zoom: 7,
    center: new google.maps.LatLng(-2.548926, 118.0148634),
  };
  const defaultBounds = new google.maps.LatLngBounds(
    new google.maps.LatLng(-11.436955, 91.142578),
    new google.maps.LatLng(5.790897, 141.503906)
  );
  var options = {
    bounds: defaultBounds,
    types: ["establishment"],
    componentRestrictions: { country: ["ID"] },
    fields: ["place_id", "geometry", "name"],
  };
  map = new google.maps.Map(document.getElementById("map"), mapOptions);
  const geocoder = new google.maps.Geocoder();
  var autocomplete = new google.maps.places.Autocomplete(input, options);
  autocomplete.setFields(["address_components", "geometry", "icon", "name"]);
  autocomplete.bindTo("bounds", map);
  var iconBase = "/assets/image/";
  let marker = new google.maps.Marker({
    map,
    anchorPoint: new google.maps.Point(0, -29),
    draggable:true,
    icon: iconBase + "Marker.png",
  });
  map.addListener("click", (center) => {
    var latLng = center.latLng;
    if (marker && marker.setMap) {
      marker.setMap(null);
    }
    marker = new google.maps.Marker({
      position: latLng,
      map: map,
      icon: iconBase + "Marker.png",
    });
    map.setPosition(latLng);
    map.setCenter(latLng);
    marker.setMap(latLng);
    geocoder.geocode(
      {
        latLng: marker.getPosition(),
      },
      function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          infoWindow.setContent(
            "<h3>" +
              marker.title +
              "</h3>" +
              data.description +
              "<br><b>address:</b> " +
              results[0].formatted_address
          );
          infoWindow.open(map, marker);
        } else {
          infoWindow.setContent(
            "<h3>" + marker.title + "</h3>" + data.description
          );
          infoWindow.open(map, marker);
        }
      }
    );
  });

  autocomplete.addListener(
    "place_changed",
    (onPlaceChanged = () => {
      infoWindow.close();
      marker.setVisible(false);
      let place = autocomplete.getPlace();

      if (!place.geometry) {
        input.placeholder = "Masukkan alamat";
        //   document.getElementById("details").innerHTML = place.name;
      }
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(14); // Why 17? Because it looks good.
      }
      marker.setPosition(place.geometry.location);
      marker.setVisible(true);
      let address = "";

      if (place.address_components) {
        address = [
          (place.address_components[0] &&
            place.address_components[0].short_name) ||
            "",
          (place.address_components[1] &&
            place.address_components[1].short_name) ||
            "",
          (place.address_components[2] &&
            place.address_components[2].short_name) ||
            "",
        ].join(" ");
      }
      infoWindow.open(marker);
    })
  );
  infoWindow = new google.maps.InfoWindow();

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        const pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        infoWindow.setPosition(pos);
        // infoWindow.open(marker);
        map.setCenter(pos);
        map.setZoom(14);
        marker = new google.maps.Marker({
          position: pos,
          map: map,
          icon: iconBase + "Marker.png",
        });
        marker.setMap(map);
      },
      () => {
        handleLocationError(true, infoWindow, map.getCenter());
      }
    );
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, infoWindow, map.getCenter());
  }
};

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(
    browserHasGeolocation
      ? "Error: The Geolocation service failed."
      : "Error: Your browser doesn't support geolocation."
  );
  infoWindow.open(map);
}

google.maps.event.addDomListener(window, "load", autocompleteSearch);
</script>
@endpush