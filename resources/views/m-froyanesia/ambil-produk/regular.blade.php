<div class="modal fade" id="regular" data-keyboard="false" tabindex="-1" aria-labelledby="regularLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mx-auto">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title">Reguler</h6>
            </div>
            <div class="modal-body text-center ">
                <input type="text" name="date" id="date-pick" class="input-custom">
                @foreach($deliveryTime->getData() as $item)
                    <a href="#!">
                        <div class="card py-4 time" data-time="{{$item->time}}">{{$item->time}}</div>
                    </a>
                @endforeach
            </div>
            <div class="modal-footer">
                <div class="w-100">
                    <div class="row">
                        <div class="col-6">
                            <button class="btn-cancel" data-dismiss="modal" aria-label="Close">Batal</button>
                        </div>
                        <div class="col-6">
                            <button class="btn-choose" data-dismiss="modal">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('customJs')
<script>
    const current = new Date()
    const parent2 = $('#regular')
    parent2.on('click', '.time', function() {
        const time = $(this).data('time');
        localStorage.setItem('time', time);
    })
    parent2.on('click', '.btn-choose', function() {
        localStorage.setItem('deliveryType', 'reguler');

        // datetime delivery
        let date = parent2.find('#date-pick').val();
        let time = localStorage.getItem('time')
        const dataTime = toDate(date, time)
        function toDate(date, time) {
            const now = new Date();
            const formatDate = date.split("-")
            const formatTime = time.split(":")
            console.log(formatDate[1]);
            now.setDate(formatDate[0]);
            now.setMonth(formatDate[1] - 1);
            now.setFullYear(formatDate[2]);
            now.setHours(formatTime[0]);
            now.setMinutes(formatTime[1]);
            return now;
        }
        dataTime < current ? alert("Maaf, tidak bisa memilih waktu yang sudah lewat") : localStorage.setItem('deliveryDateTime', `${date} ${time}`);
    })
</script>
@endpush