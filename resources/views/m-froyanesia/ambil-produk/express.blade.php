<div class="modal fade" id="express" data-keyboard="false" tabindex="-1" aria-labelledby="expressLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mx-auto">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title">Express</h6>
            </div>
            <div class="modal-body text-center ">
                <input type="text" name="date" id="date" class="input-custom mb-4">
                <input type="text" name="time" id="time" class="input-custom">
            </div>
            <div class="modal-footer">
                <div class="w-100">
                    <div class="row">
                        <div class="col-6">
                            <button class="btn-cancel" data-dismiss="modal" aria-label="Close">Batal</button>
                        </div>
                        <div class="col-6">
                            <button class="btn-choose" data-dismiss="modal">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('customJs')
<script>
    const parent = $('#express')
    parent.on('click', '.btn-choose', function() {
        localStorage.setItem('deliveryType', 'express');

        // datetime delivery
        let date = parent.find('#date').val();
        let time = parent.find('#time').val();
        
        localStorage.setItem('deliveryDateTime', `${date} ${time}`);
    })
</script>
@endpush