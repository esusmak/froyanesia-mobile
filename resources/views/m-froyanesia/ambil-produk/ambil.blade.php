@extends('m-froyanesia.layouts.master')
@section('page')
Lokasi Pengambilan
@endsection

@push('customCss')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush

@section('content')
    <header id="other">
        <nav class="navbar navbar-expand">
            <div class="container px-3 py-2">
                <a href="/keranjang" class="title-page"><i class="fas fa-chevron-left"></i></a>
                <h6 class="title-page mx-auto">Lokasi Pengambilan</h6>
            </div>
        </nav>
    </header>
    <main>
        <article id="lokasi-pengambilan" class="p-2">
            <div class="container">
            @include('m-froyanesia.ambil-produk.ambil-kirim-nav')
            <div class="data-lokasi px-2">
                <figure>
                    <img id="outlet-image" src="{{asset('assets/image/ambil di outlet.png')}}" alt="Outlet" class="w-100">
                </figure>
                <p class="title-location mb-2">Lokasi Saya :</p>
                <p class="info-location mb-2">Sistem akan mencari lokasi outlet terdekat</p>
                <form action="">
                    <input type="text" class="input-custom input-location" id="autoComplete" placeholder="Ngijo">
                    <input type="submit" class="d-none" value="">
                </form>
                <p class="title-location mt-4 mb-2">Outlet :</p>
                <select name="" id="outlets" class="input-custom">
                    
                </select>
                {{-- <p class="title-location mt-4">Alamat Outlet :</p>
                <div class="card p-4 mt-2 mb-5" id="outlets">
                    
                </div> --}}
                <button class="btn-confirm mt-5" disabled>Selesai</button>
            </div>
            </div>
        </article>
    </main>
@endsection

@push('customJs')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    localStorage.clear()
    localStorage.setItem('type', 'pickup')
    $(".btn-confirm").on('click', function() {
        let deliveryCostId = localStorage.getItem('deliveryCostId')
        
        if(deliveryCostId.length < 0) {
            alert('lengkapi data pengiriman terlebih dahulu')
            return false
        }

        localStorage.setItem('orderType', 'take')

        location.href = `{!! route('checkout.index') !!}`
    })

    $( "#autoComplete" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: baseApi + `v1/villages?limit=999&sort=asc&include=district`,
                data: {
                    search: request.term
                },
                success: function( data ) {
                    let res = data.data.map((value, key) => {
                        result = {
                            "name": value.attributes.name,
                            "id": value.id
                        }
                        let included = data.included
                        
                        if(typeof value.relationships !== 'undefined' && typeof included[key] !== 'undefined') {
                            result['district'] = included[key].attributes.name
                        }
                        return result
                    }) 
                    
                    response( res );
                }
            });
        },
        minLength: 3,
        select: function( event, ui ) {
            let villageId = ui.item.id
            getOutlets(villageId)
            let label = ui.item.name +", " + ui.item.district
            $( "#autoComplete" ).val(label);
            return false;
        },
        open: function() {
            $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function() {
            $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( `<div>${item.name + ', ' + item.district}</div>`)
        .appendTo( ul );
    };

    $("#outlets").on('change', function() {
        let outletId = $(this).find(':selected').data('outletid')
        let outletImage = $(this).find(':selected').data('image')
        localStorage.setItem('outletId', outletId)
        let villageId = localStorage.getItem('villageId')
        $.ajax({
            url: baseApi + `v1/deliverycosts/self?villageId=${villageId}`,
            methods: "GET",
            success: function( data ) {
                let res = data.data
                localStorage.setItem('deliveryCostId', res.id)
                $('.btn-confirm').attr('disabled', false)
                $('#outlet-image').attr('src', outletImage)
                $
            }
        })
        
    })

    function getOutlets(villageId) {
        localStorage.setItem('villageId', villageId)
        $.ajax({
            url: baseApi + `v1/outlets?sort=desc&include=district&villageId=${villageId}`,
            success: function( data ) {
                let res = data.data
                
                if(res.length == 0) {
                    getOutletsAll()
                }else {
                    $('#outlets').empty()
                    let radio = ''
                    radio += `<option disabled selected>Pilih Outlet</option>`
                    res.map((value, key) => {
                        let name = value.attributes.name
                        let address = value.attributes.address
                        let photo = value.attributes.image
                        radio += `<option value="${value.id}" data-outletid="${value.id}"  data-image="${photo}">${name +', '+address}</option>`
                        // radio += `<div class="form-check">
                        //             <label class="form-check-label">
                        //                 <input type="radio" data-image="${photo}" class="form-check-input" data-outletid="${value.id}" name="outlets">${name +', '+address}
                        //             </label>
                        //          </div>`
                    })
                    $('#outlets').append(radio)
                }
            }
        })
    }

    function getOutletsAll() {
        $.ajax({
            url: baseApi + `v1/outlets?sort=desc&include=district`,
            success: function( data ) {
                let res = data.data
                $('#outlets').empty()
                let radio = ''
                radio += `<option disabled selected>Pilih Outlet</option>`
                res.map((value, key) => {
                    let name = value.attributes.name
                    let address = value.attributes.address
                    let photo = value.attributes.image
                    radio += `<option value="${value.id}" data-outletid="${value.id}"  data-image="${photo}">${name +', '+address}</option>`
                    // radio += `<div class="form-check">
                    //             <label class="form-check-label">
                    //                 <input type="radio" data-image="${photo}" class="form-check-input" data-outletid="${value.id}" name="outlets">${name +', '+address}
                    //             </label>
                    //          </div>`
                })
                $('#outlets').append(radio)
            }
        })
    }
</script>
@endpush