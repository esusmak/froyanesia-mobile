@extends('m-froyanesia.layouts.master')
@section('page')
Lokasi Pengambilan
@endsection

@push('customCss')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush

@section('content')
    <header id="other">
        <nav class="navbar navbar-expand">
            <div class="container px-3 py-2">
                <a href="/keranjang" class="title-page"><i class="fas fa-chevron-left"></i></a>
                <h6 class="title-page mx-auto">Lokasi Pengambilan</h6>
            </div>
        </nav>
    </header>
    <main>
        <article id="lokasi-pengambilan" class="p-2">
            <div class="container">
            @include('m-froyanesia.ambil-produk.ambil-kirim-nav')
            <div class="data-lokasi px-2">
                <figure>
                    <img src="{{asset('assets/image/Delivery Ilustration.svg')}}" alt="Delivery Ilustration" class="w-100">
                </figure>
                <div class="d-flex justify-content-between align-items-center mb-3">
                    <p class="title-location">Diantar ke</p>
                    <button class="btn-confirm kirim-button" data-toggle="modal" data-target="#alamatLain">Alamat lain</button>
                </div>
                
                {{-- <input type="text" class="input-custom input-location mb-4" id="autoComplete" placeholder="Cari Lokasi"> --}}
                
                <textarea name="deliveryAddress" id="deliveryAddress" class="input-custom" readonly>
                    {{$user->data->attributes->address}}
                </textarea>
                <p class="title-location mb-3">Waktu Antar</p>
                <div class="row">
                        <div class="col-6">
                            <button class="time-deliver"  data-toggle="modal" data-target="#express">Express</button>
                        </div>
                        <div class="col-6">
                            <button class="time-deliver" data-toggle="modal" data-target="#regular">
                                Reguler<br>(Free Ongkir)
                            </button>
                        </div>
                </div>
                    <button class="btn-confirm selesai mt-5" disabled>Selesai</button>
            </div>
            </div>
        </article>
    </main>
@include('m-froyanesia.ambil-produk.regular');
@include('m-froyanesia.ambil-produk.express');
@include('m-froyanesia.ambil-produk.alamat-lain');
@endsection

@push('customJs')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    localStorage.clear()
    localStorage.setItem('type', 'delivery')
    let checkDeliveryCostId = `{!! $deliveryCostId !!}`
    if(checkDeliveryCostId) {
        localStorage.setItem('deliveryCostId', checkDeliveryCostId)
        $(".selesai").attr('disabled', false)
    }
    
    $(".selesai").on('click', function() {
        let deliveryAddress = $("#deliveryAddress").val()
        let deliveryCostId = localStorage.getItem('deliveryCostId')
        
        if(deliveryAddress.length <= 0 || deliveryCostId.length < 0) {
            alert('Lengkapi data pengiriman terlebih dahulu')
            return false
        }
        localStorage.setItem('orderType', 'delivery')
        localStorage.setItem('deliveryAddress', deliveryAddress)
        location.href = `{!! route('checkout.index') !!}`
    })

    

    function setDeliveryCost(villageId) {
        $.ajax({
            url: baseApi + `v1/deliverycosts/self?villageId=${villageId}`,
            success: function( data ) {
                let res = data.data
                let cost = typeof res.id !== 'undefined' ? res.id :  0
                localStorage.setItem('deliveryCostId', cost)
                $(".selesai").attr('disabled', false)
            }
        })
    }
</script>
@endpush