@extends('m-froyanesia.layouts.master')
@section('page')
    Checkout
@endsection
@section('content')
    <header id="other">
        <nav class="navbar navbar-expand">
            <div class="container px-3 py-2">
                <a href="/ambil" class="title-page"><i class="fas fa-chevron-left"></i></a>
                <h6 class="title-page mx-auto">Checkout</h6>
            </div>
        </nav>
    </header>
    <main>
        <div class="container">
            <article id="bill" class="px-4 py-3">
                <h4 class="bill-title mb-4">Pesanan</h4>
                <section id="bill-list" class="mb-4">
                    @foreach ($carts as $item)
                        <div class="card border-0 p-2 mb-3">
                            <div class="row">
                                <div class="col-4">
                                    @if (!empty($item->variant->product->productimages))
                                        <figure class="m-0">
                                            <img src="{{ $item->variant->product->productimages[0]->file }}"
                                                alt="{{ $item->variant->product->name }}" class="img-fluid">
                                        </figure>
                                    @endif
                                </div>
                                <div class="col-4 px-1">
                                    <h5 class="bill-name pt-3">{{ $item->variant->product->name }}</h5>
                                    <p class="bill-weight">{{ $item->variant->product->description }}</p>
                                    <p class="bill-price">Rp  {{ number_format($item->totalPrice) }}</p>
                                </div>
                                <div class="col-4 py-3">
                                    <div class="bill-qty">
                                        <h3>{{ $item->qty }}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </section>
                <section id="totalBill">
                    <h5 class="bill-title">Total Semua</h5>
                    <div class="data-bill py-3">
                        <div class="d-flex justify-content-between mb-2">
                            <div class="total-title">
                                <p>Subtotal</p>
                            </div>
                            <div class="total-nominal">
                                <p id="totalItem"></p>
                            </div>
                        </div>
                        @if(isset($calculate->included))
                            <div class="total-title">
                                <p>Diskon atau Free :</p>
                            </div>
                            @foreach($calculate->included as $item)
                                <div class="d-flex justify-content-between mb-2">
                                    <div class="total-title">
                                        <p id="discount-label">
                                            {{$item->attributes->description ."({$item->attributes->product_name})"}} :
                                        </p>
                                    </div>
                                    <div class="total-nominal">
                                        <p id="discount">
                                            @if($item->attributes->discount_rule_id == 1 || $item->attributes->discount_rule_id == 2)
                                                -Rp {{number_format($item->attributes->discountCalculate)}}
                                            @else
                                                -Rp {{number_format($item->attributes->discount_ldr)}}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        <div class="d-flex justify-content-between">
                            <div class="total-title">
                                <p>Ongkir</p>
                            </div>
                            <div class="total-nominal">
                                <p id="ongkir"></p>
                            </div>
                        </div>
                    </div>
                    <div class="bill-payment mt-3">
                        <div class="d-flex justify-content-between">
                            <p class="pay-total">Total</p>
                            <p class="pay-total" id="totalPrice"></p>
                        </div>
                    </div>
                </section>
                <section id="payment-method" class="mt-4">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h4 class="bill-title">Metode Pembayaran</h4>

                        {{-- Dibawah ini adalah button Edit --}}
                        {{-- <button class="btn-confirm" data-toggle="modal" data-target="#ganti-metode">Ubah</button> --}}
                    </div>
                    <button class="btn-choose-payment mb-3" data-toggle="modal"
                        data-target="#transfer-method">Transfer</button>
                    {{-- <button class="btn-choose-payment" data-toggle="modal" data-target="#lainnya">Lainnya</button> --}}
                    <div id="lainnya">
                        @foreach ($accountBankLain as $item)
                            <a href="#!" data-bank_id="{{ $item->id }}" class="btn-bank"
                                data-payment_method_id="{{ $item->paymentMethodId }}">
                                <div class="card p-2 mb-3">
                                    <figure>
                                        <img src="{{ $item->logo }}" alt="{{ $item->name }}" class="img-fluid">
                                    </figure>
                                    <div class="info">
                                        <p class="title mb-2">
                                            {{ $item->name }}
                                            @if ($item->type_bank_payment == 'payment_manual')
                                                (Dicek Manual)
                                            @elseif($item->type_bank_payment == 'payment_gateway')
                                                (Dicek Otomatis)
                                            @endif
                                        </p>
                                        <p class="content">Hanya menerima dari bank {{ $item->name }}</p>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                    {{-- Dibawah ini adalah tampilan setelah pilih metode --}}
                    {{-- <div id="choosen-method" class="mt-4">
                        <div class="card p-2">
                            <div class="row">
                                <div class="col-4 col-sm-3 p-0 pr-1">
                                    <figure class="m-0">
                                        <img src="{{asset('assets/image/Icons/BRI.svg')}}" alt="BRI" class="img-payment">
                                    </figure>
                                </div>
                                <div class="col-8 col-sm-9 p-0 pl-1">
                                    <div class="bank">
                                        <h6 class="bank-title">Bank BRI (Dicek Manual)</h6>
                                        <p class="keterangan mb-0">Hanya menerima dari bank BRI biaya penagangan Rp1.000</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </section>
                <button class="btn-confirm mt-4" id="btn-bayar" disabled>Proses</button>
            </article>
        </div>
    </main>
    @include('m-froyanesia.pesanan.ganti-metode')
    @include('m-froyanesia.pesanan.list-transfer')
    @include('m-froyanesia.pesanan.lainnya')

    @if($calculate->data->attributes->isCanClaim)
        @include('m-froyanesia.transaksi.diskonModal', ['calculate' => $calculate])
    @endif
@endsection

@push('customJs')
@if($calculate->data->attributes->isCanClaim)
<script>
    $('#diskonModal').modal('show');
    $('#diskonModal').on('click', '.btn-claim', function() {
        const status = $(this).data('status');
        localStorage.setItem('claimStatus', status);
    })
</script>
@endif
<script>
    let orderType = localStorage.getItem('orderType')

    var body 
    if(localStorage.getItem('deliveryCostId') == 0 || localStorage.getItem('deliveryType') == 'reguler'){
        body = {
            "type": "transactions",
            "attributes": {
               
            }
        }
    }else{
        body = {
            "type": "transactions",
            "attributes": {
                "deliveryCostId": localStorage.getItem('deliveryCostId')
            }
        }
    }
    
    if (
        localStorage.getItem('type') == 'pickup'
    ) {
        body['attributes']['deliveryCostId'] = null
    }
    
    if (localStorage.getItem('deliveryCostId') === null) {
        Swal.fire({
            title: `Pilih tipe order terlebih dahulu`,
            icon: 'error',
            confirmButtonText: "ok"
        })
        let redirect = () => {
            location.href = `{!! route('order.take') !!}`
        }
        setTimeout(redirect, 1000)
    }

    if (typeof orderType === 'undefined') {
        Swal.fire({
            title: `Lengkapi data pengiriman terlebih dahulu`,
            icon: 'danger',
            confirmButtonText: "ok"
        })
        let redirect = () => {
            location.href = `{!! route('order.take') !!}`
        }
        setTimeout(redirect, 4000)
    }

    if (
        typeof localStorage.getItem('voucherId') !== 'undefined' ||
        localStorage.getItem('voucherId') !== null
    ) {
        body['attributes']['voucherId'] = localStorage.getItem('voucherId')
    }

    function format_rupiah(angka, prefix = "Rp"){
			var number_string = angka.toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? ',' : '';
				rupiah += separator + ribuan.join(',');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
	}

    $.ajax({
        url: baseApi + "v1/transactions/calculate",
        method: "POST",
        data: body,
        headers: {
            'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
        },
        success: function(data) {
            let res = data.data
            
            if (res.attributes.totalAll == 0) {
                location.href = `{!! route('public.home') !!}`
            }
            // if (res.attributes.discount > 0) {
            //     $('#discount').text(`-Rp ${res.attributes.discount}`)
            // }

            $('#totalItem').text(format_rupiah(`${res.attributes.totalItem}`))
            if(localStorage.getItem('type') != 'pickup') {
                $('#ongkir').text(format_rupiah(`${res.attributes.deliveryCost}`))
            } else {
                $('#ongkir').text(format_rupiah(0))
            }
            $('#totalPrice').text(format_rupiah(`${res.attributes.totalAll}`))
        },
        error: function(request, textStatus, errorThrown) {
            let msg = JSON.parse(request.responseText)
            msg = msg.errors[0].title
            Swal.fire({
                title: `${msg}`,
                icon: 'error',
                confirmButtonText: "ok"
            })
            let redirect = () => {
                location.href = `{!! route('cart.index') !!}`
            }
            setTimeout(redirect, 1000)

        }
    })

    $('#transfer-method').add('#lainnya').on('click', '.btn-bank', function() {
        let bankId = $(this).data('bank_id')
        let paymentMethodId = $(this).data('payment_method_id')
        if(localStorage.getItem('type') == 'pickup' && paymentMethodId == 3) {
            alert('Metode pembayaran COD hanya berlaku untuk transaksi pengiriman saja')
            return false;
        }
        
        localStorage.setItem('bankId', bankId)
        localStorage.setItem('paymentMethodId', paymentMethodId)
        $('#btn-bayar').attr('disabled', false)
        $('#lainnya').modal('hide')
        $('#transfer-method').modal('hide')
    })

    $('#btn-bayar').on('click', function() {
        let bankId = localStorage.getItem('bankId')
        let deliveryCostId = localStorage.getItem('deliveryCostId')

        var body
        if(localStorage.getItem('deliveryCostId') == 0){
            body = {
                "type": "transactions",
                "attributes": {
                    "bankId": bankId,
                    "type": localStorage.getItem('type')
                }
            }
        }else{
            body = {
                "type": "transactions",
                "attributes": {
                    "bankId": bankId,
                    "deliveryCostId": deliveryCostId,
                    "type": localStorage.getItem('type')
                }
            }
        }
       

        if (localStorage.getItem('type') == 'pickup') {
            body['attributes']['deliveryCostId'] = null
        }

        if (orderType == 'take') {
            let outletId = localStorage.getItem('outletId')
            body['attributes']['outletId'] = outletId
        }

        if (orderType == 'delivery') {
            let deliveryAddress = localStorage.getItem('deliveryAddress')
            body['attributes']['deliveryAddress'] = deliveryAddress
        }
        let voucherCode = localStorage.getItem('voucherCode')

        if (typeof voucherCode !== 'undefined') {
            body['attributes']['voucherCode'] = voucherCode
        }

        if (
            typeof localStorage.getItem('deliveryType') !== 'undefined' ||
            localStorage.getItem('deliveryType') !== null
        ) {
            body['attributes']['deliveryType'] = localStorage.getItem('deliveryType')
        }

        if (
            typeof localStorage.getItem('deliveryDateTime') !== 'undefined' ||
            localStorage.getItem('deliveryDateTime') !== null
        ) {
            body['attributes']['deliveryDateTime'] = localStorage.getItem('deliveryDateTime')
        }

        if (
            typeof localStorage.getItem('latitude') !== 'undefined' ||
            localStorage.getItem('latitude') !== null
        ) {
            body['attributes']['latitude'] = localStorage.getItem('latitude')
        }

        if (
            typeof localStorage.getItem('longitude') !== 'undefined' ||
            localStorage.getItem('longitude') !== null
        ) {
            body['attributes']['longitude'] = localStorage.getItem('longitude')
        }

        if (
            typeof localStorage.getItem('paymentMethodId') !== 'undefined' ||
            localStorage.getItem('paymentMethodId') !== null
        ) {
            body['attributes']['paymentMethodId'] = localStorage.getItem('paymentMethodId')
        }

        if (
            typeof localStorage.getItem('recipientName') !== 'undefined' ||
            localStorage.getItem('recipientName') !== null
        ) {
            body['attributes']['recipientName'] = localStorage.getItem('recipientName')
        }

        // Checkout 
        $.ajax({
            url: baseApi + "v1/transactions/checkout",
            method: "POST",
            headers: {
                'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
            },
            data: body,
            success: function(data) {
                let msg = data.meta
                let res = data.data
                
                // claim API
                if(localStorage.getItem('claimStatus') == 1 || localStorage.getItem('claimStatus') == 0) {
                    const claimStatus = localStorage.getItem('claimStatus')
                    const bodyClaim = {
                        "type": "transactions",
                        "attributes": {
                            "take": claimStatus
                        }
                    }
                    
                    $.ajax({
                        url: baseApi + `v1/transactions/${res.id}/takegift`,
                        method: "PUT",
                        headers: {
                            'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
                        },
                        data: bodyClaim,
                        success: function(data) {
                            localStorage.clear()
                            Swal.fire({
                                title: `${msg.message}`,
                                icon: 'success',
                                confirmButtonText: "ok"
                            })
                            let redirect = () => {
                                location.href = `/pembayaran/${res.id}`
                            }
                            setTimeout(redirect, 1000)
                        }
                    })
                } else {
                    localStorage.clear()
                    Swal.fire({
                        title: `${msg.message}`,
                        icon: 'success',
                        confirmButtonText: "ok"
                    })
                    let redirect = () => {
                        location.href = `/pembayaran/${res.id}`
                    }
                    setTimeout(redirect, 1000)
                }

                
            },
            error: function(request, textStatus, errorThrown) {
                let msg = JSON.parse(request.responseText)
                Swal.fire({
                    title: `${msg.errors[0].title}`,
                    icon: 'error',
                    confirmButtonText: "ok"
                })
                let redirect = () => {
                    location.href = `{!! route('public.home') !!}`
                }
                setTimeout(redirect, 1000)
            }
        })
    })

</script>
@endpush
