<section id="evidence">
    <div class="image-upload-field">
        <input name="file" class="file-upload-input" type='file' onchange="uploadImage(this);" accept="image/*" />
        @if(empty($transaction->proofOfPayment))
        <div class="icons-upload">
            <img src="{{asset('assets/image/Icons/Upload.svg')}}" alt="Upload or Drag Your Image">
            <h6 class="mt-4">Upload Bukti Pembayaran Kamu Disini</h6>
        </div>
        @else
        <div class="text-center">
            <img src="{{$transaction->proofOfPayment}}" class="file-upload-image">
        </div>
        @endif
    </div>
    <div class="file-upload-content">
        <img class="file-upload-image" src="#" alt="your image" />
        <div class="image-title-wrap">
            <p class="image-title"></p>
            <button onclick="$('.file-upload-input').trigger( 'click' )" class="change-image">Ubah Foto</button>
        </div>
    </div>
</section>