<div class="container">
    <article id="payment" class="px-4 py-3">
        <section id="method" class="mb-4">
            <div class="d-flex justify-content-between align-items-center mb-3">
                <h4 class="payment-title">Metode Pembayaran</h4>
                @if ($transaction->currentStatus == 'PENDING' && $transaction->paymentStatus == 'PENDING')
                    <button class="btn-confirm" data-toggle="modal" data-target="#ganti-metode">Ubah</button>
                @endif
            </div>
            <div class="card border-0 px-4 py-3">
                <div class="d-flex justify-content-between align-items-center">
                    <figure>
                        <img src="{{ asset('assets/image/Icons/Salary.svg') }}" alt="Salary" class="img-fluid">
                    </figure>
                    <h6 class="mx-auto">Bayar ditempat</h6>
                </div>
            </div>
        </section>
        <section id="payment-bill">
            <h4 class="payment-title mb-3">Jumlah Tagihan</h4>
            <div class="card border-0 px-3 py-4">
                <div class="d-flex justify-content-between">
                    <h6>Total</h6>
                    <h6>Rp  {{ number_format($transaction->totalBuying) }}</h6>
                </div>
            </div>
        </section>
        <div id="cod" class="row">
            <div class="col-6">
                <a href="/">
                    <button class="btn-confirm-one">Beranda</button>
                </a>
            </div>
            <div class="col-6">
                <a href="{{ route('transaction.index') }}">
                    <button class="btn-confirm">Lihat Transaksi</button>
                </a>
            </div>
        </div>
    </article>
</div>
