<div class="modal fade" id="transfer-method" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable mx-auto">
        <div class="modal-content">
            <div class="modal-body">
                @foreach ($accountBankTransfer as $item)
                <a href="#!" data-bank_id="{{$item->id}}" class="btn-bank" data-payment_method_id="{{$item->paymentMethodId}}">
                    <div class="card mb-3 p-2">
                        <figure>
                            <img src="{{$item->logo}}" alt="{{$item->name}}" class="img-fluid">
                        </figure>
                        <div class="info">
                            <p class="title mb-2">
                                {{$item->name}} 
                                @if($item->type_bank_payment == 'payment_manual')
                                (Dicek Manual)
                                @elseif($item->type_bank_payment == 'payment_gateway')
                                (Dicek Otomatis)
                                @endif
                            </p>
                            <p class="content">Hanya menerima dari bank {{$item->name}}</p>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>
</div>