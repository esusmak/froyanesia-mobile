<div class="container">
    <article id="payment" class="px-4 py-3">
        <section id="countdown" class="mb-4">
            <h4 class="payment-title mb-3">Batas Pembayaran</h4>
            <h1 class="date py-3 text-center" style="font-size: 18px">{{date('d F Y H:i:s', strtotime($transaction->expiryDate))}}</h1>
        </section>
        <section id="method" class="mb-4">
            <div class="d-flex justify-content-between align-items-center mb-3">
                <h4 class="payment-title">Metode Pembayaran</h4>
                <button class="btn-confirm"  data-toggle="modal" data-target="#ganti-metode">Ubah</button>
            </div>
             <div class="card border-0 p-3">
                <div class="d-flex justify-content-between">
                    <div class="bank-account">
                        <figure>
                            <img src="{{$transaction->bank->logo}}" alt="{{$transaction->bank->name}}" class="img-bank">
                        </figure>
                        <p class="account-name">a/n: {{$transaction->bank->person_name}}</p>
                        <p class="account-number mb-0" id="norek">{{$transaction->bank->no_rek}}</p>
                    </div>
                    <figure class="d-flex mb-0">
                        <img src="{{asset('assets/image/Icons/Copy.svg')}}" onclick="copyToClipboard('#norek')" alt="Copy to Clipboard" class="img-copy mt-auto align-self-end">
                    </figure>
                </div>
            </div>
        </section>
        <section id="payment-bill" class="mb-4">
            <h4 class="payment-title mb-3">Jumlah Tagihan</h4>
            <div class="card border-0 px-3 py-4">
                <div class="d-flex justify-content-between">
                    <h6>Total</h6>
                    <h6>Rp  {{number_format($transaction->totalBuying)}}</h6>
                </div>
            </div>
        </section>
        <form action="{{route('transaction.pembayaran', $id)}}" method="post" enctype="multipart/form-data">
            <section id="tglTransfer" class="mb-4">
                <h4 class="payment-title mb-3">Tanggal Transfer</h4>
                <figure class="img-calendar">
                    <img src="{{asset('assets/image/Icons/Calendar.svg')}}" alt="Calendar" class="img-fluid">
                </figure>
                <input type="text" class="input-custom" required id="tgl_transfer" name="transferDate" value="{{$transaction->popTransferDate != null ? date('Y-m-d H:i:s', strtotime($transaction->popTransferDate)) : ''}}">
            </section>
            <section id="bankPengirim" class="mb-4">
                <h4 class="payment-title mb-3">Bank Pengirim</h4>
                <input type="text" class="input-custom" required id="bank_pengirim" name="bankName" value="{{$transaction->popBankName}}">
            </section>
            <section id="noRekening" class="mb-4">
                <h4 class="payment-title mb-3">No. Rekening</h4>
                <input type="text" class="input-custom" name="bankNo" required id="no_rekening" value="{{$transaction->popBankNo}}">
            </section>
            <section id="namaPengirim" class="mb-4">
                <h4 class="payment-title mb-3">Nama Pengirim</h4>
                <input type="text" class="input-custom" required id="nama_pengirim" value="{{$transaction->popPersonName}}" name="personName">
            </section>
            <h4 class="payment-title mb-3">Upload Bukti</h4>
            @csrf
            @include('m-froyanesia.pesanan.upload-bukti')
            <button type="submit" class="btn-confirm" id="konfirmasi" disabled>Konfirmasi Pembayaran</button>
        </form>
    </article>
</div>

@push('customJs')
    <script>     
        function uploadImage (input) {
            if (input.files && input.files[0]) {

                var reader = new FileReader();

                reader.onload = function(e) {
                    $('.image-upload-field').hide();

                    $('.file-upload-image').attr('src', e.target.result);
                    $('.file-upload-content').show();

                    $('.image-title').html(input.files[0].name);
                };

                reader.readAsDataURL(input.files[0]);
                $('#konfirmasi').prop('disabled', false)
            } else {
                removeUpload()
                $('#konfirmasi').prop('disabled', true)
                $('.image-upload-field').show();
            }
        }
        function removeUpload() {
            $('.file-upload-input').replaceWith($('.file-upload-input').clone());
            $('.file-upload-content').hide();
            $('.image-upload-field').show();
            }

            $('.image-upload-field').bind('dragover', function () {
                $('.image-upload-field').addClass('image-dropping');
            });
            $('.image-upload-field').bind('dragleave', function () {
                $('.image-upload-field').removeClass('image-dropping');
            });
       
        function copyTextToClipboard(text) {
            if (!navigator.clipboard) {
                fallbackCopyTextToClipboard(text);
                return;
            }
            navigator.clipboard.writeText(text).then(function() {
                console.log('Async: Copying to clipboard was successful!');
            }, function(err) {
                console.error('Async: Could not copy text: ', err);
            });
        }

        function copyToClipboard(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).text()).select();
            document.execCommand("copy");
            $temp.remove();
            Swal.fire({
                icon: 'success',
                title: `nomor rekening berhasil dicopy`,
                width : 400
            })
            
        }
    </script>
@endpush