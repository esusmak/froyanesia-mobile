@extends('m-froyanesia.layouts.master')
@section('page')
    Pembayaran
@endsection
@section('content')
    <header id="other">
        <nav class="navbar navbar-expand">
            <div class="container px-3 py-2">
                <a href="{{route('transaction.index')}}" class="title-page"><i class="fas fa-chevron-left"></i></a>
                <h6 class="title-page mx-auto">Pembayaran</h6>
            </div>
        </nav>
    </header>
    <main>
        @if($transaction->paymentMethod == 'COD' || $transaction->paymentMethod == 'CASH')
            @include('m-froyanesia.pesanan.cod')
        @elseif($transaction->paymentMethod == 'BANK')
            @include('m-froyanesia.pesanan.transfer')
        @endif
    </main>
@include('m-froyanesia.pesanan.konfirmasi-pembayaran')
@include('m-froyanesia.pesanan.ganti-metode')
@endsection

