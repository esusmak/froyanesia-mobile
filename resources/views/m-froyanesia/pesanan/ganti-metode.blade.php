<div class="modal fade" id="ganti-metode" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable mx-auto">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title">Metode Pembayaran</h6>
            </div>
            <div class="modal-body">
                <div class="accordion" id="accordionPayment">
                    <div class="card border-0">
                        <div class="card-header mb-3" id="headingOne">
                            <h2>
                                <button class="btn-transparent btn-block py-3" type="button" data-toggle="collapse" data-target="#collapseTransfer" aria-expanded="true" aria-controls="collapseTransfer">
                                    Transfer
                                </button>
                            </h2>
                        </div>
                  
                        <div id="collapseTransfer" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionPayment">
                            <div class="card-body p-0">
                                @foreach ($accountBankTransfer as $item)
                                <a href="#!" data-bank_id="{{$item->id}}" class="btn-bank">
                                    <div class="card p-2">
                                        <figure>
                                            <img src="{{$item->logo}}" alt="{{$item->name}}" class="img-fluid">
                                        </figure>
                                        <div class="info">
                                            <p class="title mb-2">
                                                {{$item->name}} 
                                                @if($item->type_bank_payment == 'payment_manual')
                                                (Dicek Manual)
                                                @elseif($item->type_bank_payment == 'payment_gateway')
                                                (Dicek Otomatis)
                                                @endif
                                            </p>
                                            <p class="content">Hanya menerima dari bank {{$item->name}}</p>
                                        </div>
                                    </div>
                                </a>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    @foreach ($accountBankLain as $item)
                    <a href="#!" data-bank_id="{{$item->id}}" class="btn-bank">
                        <div class="card my-3 p-2">
                            <figure>
                                <img src="{{$item->logo}}" alt="{{$item->name}}" class="img-fluid">
                            </figure>
                            <div class="info">
                                <p class="title mb-2">
                                    {{$item->name}} 
                                    @if($item->type_bank_payment == 'payment_manual')
                                    (Dicek Manual)
                                    @elseif($item->type_bank_payment == 'payment_gateway')
                                    (Dicek Otomatis)
                                    @endif
                                </p>
                                <p class="content">Hanya menerima dari bank {{$item->name}}</p>
                            </div>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@push('customJs')
<script>
    $(document).on('click', '.btn-bank', function() {
        let bankId = $(this).data('bank_id')
        // $('.btn-confirm').attr('disabled', false)
        // $('.btn-confirm').data('bank_id', bankId)
        let transactionId = `{{ empty($transaction) ? null : $transaction->id }}`
        
        if(transactionId) {
            const body = {
                "type": "transactions",
                "attributes": {
                    "bankId": bankId
                }
            }
            $.ajax({
                url: `${baseApi}v1/transactions/${transactionId}/changepaymentmethod`,
                method: 'PUT',
                data: body,
                headers: {
                    'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
                },
                success: function( data ) {
                    let msg = data.meta
                    Swal.fire({
                        title: `${msg.message}`,
                        icon: 'success',
                        confirmButtonText: "ok"
                    })
                    let redirect = () => {
                        location.reload()
                    }
                    setTimeout(redirect, 1000)
                },
                error: function(request, textStatus, errorThrown) {
                    let msg = JSON.parse(request.responseText)
                    Swal.fire({
                        title: `${msg.errors[0].title}`,
                        icon: 'error',
                        confirmButtonText: "ok"
                    })
                    let redirect = () => {
                        location.reload()
                    }
                    setTimeout(redirect, 1000)
                }
            })
        } else {
            localStorage.setItem('bankId', bankId)
        }
    })
</script>
@endpush