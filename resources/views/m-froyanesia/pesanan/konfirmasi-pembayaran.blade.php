<div class="modal fade" id="paymentConfirm" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mx-auto">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center ">
                <p>Mohon ditunggu.
                    Pembayaranmu segera
                    dikonfirmasi</p>
                <figure>
                    <img src="{{asset('assets/image/Icons/Jam Pasir.svg')}}" alt="Menunggu konfirmasi pembayaran" class="img-fluid rounded">
                </figure>
            </div>
        </div>
    </div>
</div>