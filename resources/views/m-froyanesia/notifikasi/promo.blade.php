@extends('m-froyanesia.layouts.master')
@section('page')
    Detail
@endsection
@section('content')
    <header id="other">
        <nav class="navbar navbar-expand">
            <div class="container px-3 py-2">
                <a href="/notifikasi" class="title-page"><i class="fas fa-chevron-left"></i></a>
                <h6 class="title-page mx-auto">Detail</h6>
            </div>
        </nav>
    </header>
    <main>
        <article id="detailNotifikasi">
            <section id="header" class="promo py-3">
                <div class="d-flex justify-content-center">
                    <img src="{{asset('assets/image/Icons/Discount.svg')}}" alt="Diskon" class="img-notifikasi-detail mt-1">
                    <h3 class="d-inline-block title-detail-notifikasi align-self-center mt-1 ml-3">Voucher Baru!</h3>
                </div>
            </section>
            <div class="container">
                <div class="p-3">
                    <p class="content-detail-notifikasi">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor vel egestas eu dictum sit bibendum nulla auctor. Habitant nec, et felis suspendisse enim id donec orci. Sit consectetur nisl dignissim vel sed ut dui eu mattis.</p>
                    <figure class="w-100 px-4 text-center">
                        <img src="{{asset('assets/image/Gambar Voucher.svg')}}" alt="Voucher" class="img-fluid rounded">
                    </figure>
                    <p class="decsription">Voucher ini berlaku di kota :</p>
                    <ol type="1" class="px-3">
                        <li class="decsription">Surabaya</li>
                        <li class="decsription">Medan</li>
                        <li class="decsription">Jabodetabek</li>
                        <li class="decsription">Aceh</li>
                        <li class="decsription">Manado</li>
                    </ol>
                    <p class="decsription mt-5">Berlaku hingga tanggal 27 November 2020</p>
                </div>
            </div>
        </article>
    </main>
@endsection