@extends('m-froyanesia.layouts.master')
@section('page')
    Detail
@endsection
@section('content')
        @php
            $notificationStatus = [
                \App\Constants\TransactionStatusConst::TRANS_STATUS_PENDING => [
                    'type' => 'pending',
                    'icon' => asset('assets/image/Icons/Cash-payment.svg')
                ],
                \App\Constants\TransactionStatusConst::TRANS_STATUS_PAID => [
                    'type' => 'diterima',
                    'icon' => asset('assets/image/Icons/Checklist.svg'),
                ],
                \App\Constants\TransactionStatusConst::TRANS_STATUS_DELIVERING => [
                    'type' => 'diantar',
                    'icon' => asset('assets/image/Icons/Delivery-Truck.svg.svg'),
                ],
                \App\Constants\TransactionStatusConst::TRANS_STATUS_PROCESSED => [
                    'type' => 'diproses',
                    'icon' => asset('assets/image/Icons/Jam Pasir-notif.svg'),
                ],
                \App\Constants\TransactionStatusConst::TRANS_STATUS_FINISHED => [
                    'type' => 'done',
                    'icon' => asset('assets/image/Icons/Box.svg'),
                ],
                \App\Constants\TransactionStatusConst::TRANS_STATUS_CANCEL_BY_SYSTEM => [
                    'type' => 'canceled',
                    'icon' => asset('assets/image/Icons/Cross.svg'),
                ],
                \App\Constants\TransactionStatusConst::TRANS_STATUS_CANCEL_BY_ADMIN => [
                    'type' => 'ditolak',
                    'icon' => asset('assets/image/Icons/Cross.svg'),
                ],
                \App\Constants\TransactionStatusConst::TRANS_STATUS_FINISH_BY_TRANSPORTER => [
                    'type' => 'done',
                    'icon' => asset('assets/image/Icons/box.svg'),
                ],
                \App\Constants\TransactionStatusConst::TRANS_STATUS_PROCESSED_BY_OUTLET => [
                    'type' => 'diproses',
                    'icon' => asset('assets/image/Icons/Jam Pasir-notif.svg'),
                ]
            ];

            $class = null;
            foreach ($notificationStatus as $key => $value) {
                if($key == $notification->body->status) {
                    $class =  $value;
                }
            }
        @endphp
    <header>
        <nav class="navbar navbar-expand">
            <div class="container p-3">
                <a href="/notifikasi" class="title-page"><i class="fas fa-chevron-left"></i></a>
                <h6 class="title-page mx-auto">Detail</h6>
            </div>
        </nav>
    </header>
    <main>
        <article id="detailNotifikasi">
            <section id="header" class="py-3">
                <div class="d-flex justify-content-center">
                    <img src="{{$class['icon'] ?? null}}" class="img-notifikasi-detail mt-1">
                    <h3 class="d-inline-block title-detail-notifikasi align-self-center mt-1 ml-3">{{$notification->title}}</h3>
                </div>
            </section>
            <div class="container">
                <div class="p-3 content-detail-notifikasi">
                        {!!$notification->content!!}

                    @if($notification->notifType == 'voucher')
                    <p class="decsription mt-5">Berlaku hingga tanggal 27 November 2020</p>
                    @endif
                </div>
            </div>
        </article>
    </main>
    
    <footer class="p-4">
        <div class="container">
            <a href="{{route('transaction.show', $notification->body->id)}}">
                <button class="btn-confirm">Lihat Detail</button>
            </a>
            {{-- @if($class['type'] == 'done')
                <button class="btn-confirm" data-transactionid="{{$notification->body->id}}" data-type="{{$class['type']}}">Konfirmasi</button>
            @elseif ($class['type'] == 'pending')
                <a href="{{route('transaction.pembayaran', $notification->body->id)}}">
                    <button class="btn-confirm" data-type="{{$class['type']}}">Bayar</button>
                </a>
            @endif --}}
        </div>
    </footer>
@endsection

@push('customJs')
<script>
    $('.btn-confirm').on('click', function() {
        const type = $(this).data('type')
        const transactionid = $(this).data('transactionid')

        if(transactionid) {
            const body = {
                "type": "transactions",
                "attributes": {
                    "status": "FINISHED" 
                }
            }
            console.log(transactionid)
            $.ajax({
                url: `${baseApi}v1/transactions/${transactionid}/changeStatus`,
                method: 'PUT',
                headers: {
                    'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
                },
                data: body,
                success: function( data ) {
                    let msg = "Pesananmu berhasil di konfirmasi"
                    flashAlert(msg, 'info')
                    let redirect = () => {
                        location.href = `{!! route('public.home') !!}`
                    }
                    setTimeout(redirect, 1000)
                },
                error: function(request, textStatus, errorThrown) {
                    let msg = JSON.parse(request.responseText)
                    Swal.fire({
                        title: `${msg.errors[0].title}`,
                        icon: 'error',
                        confirmButtonText: "ok"
                    })
                    let redirect = () => {
                        location.href = `{!! route('public.home') !!}`
                    }
                    setTimeout(redirect, 1000)
                }
            })
        }
    })
</script>
@endpush