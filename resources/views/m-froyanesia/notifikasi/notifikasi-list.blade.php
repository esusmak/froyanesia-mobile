<article id="listNotifikasi" class="pt-1 px-3">
    <div class="container" id="notificationAll">
        @forelse ($notification->getData() as $item)
        <a href="/notifikasi/{{$item->id}}">
            @php
                $notificationStatus = [
                    \App\Constants\TransactionStatusConst::TRANS_STATUS_PENDING => [
                        'type' => 'pending',
                        'icon' => asset('assets/image/Icons/Cash-payment.svg')
                    ],
                    \App\Constants\TransactionStatusConst::TRANS_STATUS_PAID => [
                        'type' => 'diterima',
                        'icon' => asset('assets/image/Icons/Checklist.svg'),
                    ],
                    \App\Constants\TransactionStatusConst::TRANS_STATUS_DELIVERING => [
                        'type' => 'diantar',
                        'icon' => asset('assets/image/Icons/Delivery-Truck.svg.svg'),
                    ],
                    \App\Constants\TransactionStatusConst::TRANS_STATUS_PROCESSED => [
                        'type' => 'diproses',
                        'icon' => asset('assets/image/Icons/Jam Pasir-notif.svg'),
                    ],
                    \App\Constants\TransactionStatusConst::TRANS_STATUS_FINISHED => [
                        'type' => 'done',
                        'icon' => asset('assets/image/Icons/Box.svg'),
                    ],
                    \App\Constants\TransactionStatusConst::TRANS_STATUS_CANCEL_BY_SYSTEM => [
                        'type' => 'canceled',
                        'icon' => asset('assets/image/Icons/Cross.svg'),
                    ],
                    \App\Constants\TransactionStatusConst::TRANS_STATUS_CANCEL_BY_ADMIN => [
                        'type' => 'ditolak',
                        'icon' => asset('assets/image/Icons/Cross.svg'),
                    ],
                    \App\Constants\TransactionStatusConst::TRANS_STATUS_FINISH_BY_TRANSPORTER => [
                        'type' => 'done',
                        'icon' => asset('assets/image/Icons/box.svg'),
                    ],
                    \App\Constants\TransactionStatusConst::TRANS_STATUS_PROCESSED_BY_OUTLET => [
                        'type' => 'diproses',
                        'icon' => asset('assets/image/Icons/Jam Pasir-notif.svg'),
                    ]
                ];

                $class = null;
                foreach ($notificationStatus as $key => $value) {
                    if($key == $item->body->status) {
                        if($item->isRead) {
                            $value['type'] = 'readed';
                            $class =  $value;
                        }
                        $class =  $value;
                    }
                }
                
            @endphp
            <div class="card {{$class['type'] ?? null}} mb-3 p-3">
                <div class="row">
                    <div class="col-3 p-0">
                        <figure class="figure-notifikasi m-0">
                            <img src="{{$class['icon'] ?? null}}" class="img-notifikasi">
                        </figure>
                    </div>
                    <div class="col-9 p-0 pl-3">
                        <h5 class="title-list-notifikasi">{{$item->title}}</h5>
                        <p class="content-list-notifikasi">
                            @php
                                $content = strlen($item->content) > 45 ? substr($item->content,0,45)."..." : $item->content;
                                echo $content;
                            @endphp
                        </p>
                    </div>
                </div>
            </div>
        </a>
        @empty
            <div class="card text-center border-0 readed mb-3 p-3">
                <div class="row">
                    <div class="col-12 p-0 pl-3">
                        <h4 class="title-list-notifikasi text-secondary">Tidak ada notifikasi baru</h4>
                        {{-- <p class="content-list-notifikasi">Voucher baru telah tiba! Karen masa pandemi mak..</p> --}}
                    </div>
                </div>
            </div>
        @endforelse
        {{-- <div class="row">
            @if (isset($notification->getLinks()->next))
            <div class="col-md-12 text-center">
                <button class="see-more btn btn-sm btn-primary" data-link="{{$notification->getLinks()->next->getHref()}}" data-div="#notificationAll">Muat lebih banyak</button> 
            </div>
            @endif
        </div> --}}
    </div>
</article>

@push('customJs')
<script>
    
$(".see-more").on('click', function() {
            $div = $($(this).data('div')); //div to append
            $link = $(this).data('link'); //current URL
            $href = $link; //complete URL
            $html = ''
            $.ajax({
                url: $href,
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + {!! json_encode(session()->get('token'))!!}
                },
                success:function(data) {
                    res = data
                    res.data.forEach((value, key) => {
                        const id = value.id
                        const currentStatus = value.attributes.currentStatus
                        const payerEmail = value.attributes.payerEmail
                        const totalBuying = value.attributes.totalBuying
                        let dateCreated = new Date(value.attributes.dateCreated)
                        
                        $html += `<a href="/notifikasi/">
                                <div class="card readed mb-3 p-3">
                                    <div class="row">
                                        <div class="col-3 p-0">
                                            <figure class="figure-notifikasi m-0">
                                                <img src="{{asset('assets/image/Icons/Discount.svg')}}" alt="Diskon" class="img-notifikasi">
                                            </figure>
                                        </div>
                                        <div class="col-9 p-0 pl-3">
                                            <h5 class="title-list-notifikasi">Voucher Baru!</h5>
                                            <p class="content-list-notifikasi">Voucher baru telah tiba! Karena masa pandemi mak..</p>
                                        </div>
                                    </div>
                                </div>
                            </a>`

                    })
                    
                    $div.append($html);

                    if(!res.links.next) {
                        $('.see-more').remove()
                        return false
                    }
                    let nextUrl = res.links.next;

                    $('.see-more').data('link', nextUrl)
                }
            });
        });
</script>
@endpush