@extends('m-froyanesia.layouts.master')
@section('page')
    Detail
@endsection
@section('content')
    <header id="other">
        <nav class="navbar navbar-expand">
            <div class="container px-3 py-2">
                <a href="/notifikasi" class="title-page"><i class="fas fa-chevron-left"></i></a>
                <h6 class="title-page mx-auto">Detail</h6>
            </div>
        </nav>
    </header>
    <main>
        <article id="detailNotifikasi">
            <section id="header" class="pending py-3">
                <div class="d-flex justify-content-center">
                    <img src="{{asset('assets/image/Icons/Cash-payment.svg')}}" alt="Jangan lupa bayar" class="img-notifikasi-detail mt-1">
                    <h3 class="d-inline-block title-detail-notifikasi align-self-center mt-1 ml-3">Jangan lupa bayar</h3>
                </div>
            </section>
            <div class="container">
                <div class="p-3">
                    <p class="content-detail-notifikasi">Wah batas waktu pembayaran mau habis nih, yuk segera bayar pesananmu biar ngga dibatalin. Semakin cepat membayar, semakin cepat pula pesananmu akan datang.</p>
                </div>
            </div>
        </article>
    </main>
    <footer class="p-4">
        <div class="container">
            <a href="">
                <button class="btn-confirm">Lihat Detail</button>
            </a>
        </div>
    </footer>
@endsection