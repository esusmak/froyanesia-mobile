@extends('m-froyanesia.layouts.master')
@section('page')
    Notifikasi
@endsection
@section('content')
    <header id="notif">
        <div class="container px-3 py-2">
            <nav class="navbar navbar-expand">
                <a href="/" class="title-page"><i class="fas fa-chevron-left"></i></a>
                <h6 class="title-page mx-auto">Notifikasi</h6>
            </nav>
        </div>
        <div class="button-group px-3 py-4">
            <div class="row">
                <div class="col-6 px-2">
                    <button class="btn-confirm mark-read">Mark all as read</button>
                </div>
                <div class="col-6 px-2">
                    <button class="btn-confirm-one btn-empty">Kosongkan</button>
                </div>
            </div>
        </div>
    </header>
    <main>
        @include('m-froyanesia.notifikasi.notifikasi-list')
    </main>
@endsection

@push('customJs')
<script>
    $(".btn-empty").on("click", function() {
        $.ajax({
            url: `${baseApi}v1/notifications/deleteall`,
            method: 'DELETE',
            headers: {
                'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
            },
            success: function( data ) {
                location.reload()
            },
            error: function(request, textStatus, errorThrown) {
                let msg = JSON.parse(request.responseText)
                Swal.fire({
                    title: `${msg.errors[0].title}`,
                    icon: 'error',
                    confirmButtonText: "ok"
                })
                let redirect = () => {
                    location.reload()
                }
                setTimeout(redirect, 1000)
            }
        })
    })
    $('.mark-read').on('click', function() {
        $.ajax({
            url: `${baseApi}v1/notifications/markasread`,
            method: 'PUT',
            headers: {
                'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
            },
            success: function( data ) {
                location.reload()
            },
            error: function(request, textStatus, errorThrown) {
                let msg = JSON.parse(request.responseText)
                Swal.fire({
                    title: `${msg.errors[0].title}`,
                    icon: 'error',
                    confirmButtonText: "ok"
                })
                let redirect = () => {
                    location.reload()
                }
                setTimeout(redirect, 1000)
            }
        })
    })
</script>
@endpush