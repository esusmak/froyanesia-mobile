@extends('m-froyanesia.layouts.master')
@section('page')
    Detail
@endsection
@section('content')
    <header id="other">
        <nav class="navbar navbar-expand">
            <div class="container px-3 py-2">
                <a href="/notifikasi" class="title-page"><i class="fas fa-chevron-left"></i></a>
                <h6 class="title-page mx-auto">Detail</h6>
            </div>
        </nav>
    </header>
    <main>
        <article id="detailNotifikasi">
            <section id="header" class="dibuat py-3">
                <div class="d-flex justify-content-center">
                    <img src="{{asset('assets/image/Icons/Checklist.svg')}}" alt="Pesanan dibuat" class="img-notifikasi-detail mt-1">
                    <h3 class="d-inline-block title-detail-notifikasi align-self-center mt-1 ml-3">Pesanan dibuat</h3>
                </div>
            </section>
            <div class="container">
                <div class="p-3">
                    <p class="content-detail-notifikasi">Selamat! Pesananmu berhasil dibuat. Ayo belanja lebih banyak lagi untuk mendapatkan diskon yang lebih besar.</p>
                </div>
            </div>
        </article>
    </main>
@endsection