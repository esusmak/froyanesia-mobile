@extends('m-froyanesia.layouts.master')
@section('page')
    Keranjang
@endsection
@section('content')
    <header>
        <nav class="navbar navbar-expand">
            <div class="container px-3 py-2">
                <a href="/" class="title-page"><i class="fas fa-chevron-left"></i></a>
                <h6 class="title-page mx-auto">Keranjang</h6>
            </div>
        </nav>
    </header>
    <main>
        <div class="container">
            @include('m-froyanesia.cart.cart-list')
        </div>
    </main>
    <footer>
        <div id="total" class="d-flex flex-column">
            {{-- <button class="btn-voucher ml-auto"  data-toggle="modal" data-target="#voucher">
                    <figure>
                        <img src="{{asset('assets/image/Icons/Voucher.svg')}}" alt="Voucher" class="img-fluid">
                    </figure>
                </button> --}}
            <div class="applied"></div>
            @if($calculate->data->attributes->totalSaving > 0)
            <div class="d-flex justify-content-between my-2 px-2">
                <p class="total-hemat-title">Total Hemat :</p>
                <p class="total-hemat-nominal" id="totalHemat">Rp {{number_format($calculate->data->attributes->totalSaving ?? 0)}}</p>
            </div>
            @endif
            <div class="line"></div>
            <div class="d-flex justify-content-between my-3 px-2">
                <p class="total-title">Total :</p>
                <p class="total-nominal" id="totalCart">Rp 0</p>
            </div>
            <a href="/ambil" class="mb-4" id="btn-confirm">
                <button class="btn-confirm mx-auto">Checkout</button>
            </a>
        </div>
        @include('m-froyanesia.partials.bot-nav')
    </footer>
    @include('m-froyanesia.voucher.voucher-list')
@endsection

@push('customJs')
    <script>
        const countCart = `{!! $countCart !!}`
        if (countCart == 0) {
            $('.btn-voucher').attr('disabled', true)
            $('#btn-confirm').removeAttr('href')
            $('.btn-confirm').attr('disabled', true)
        }

        function format_rupiah(angka, prefix = "Rp"){
			var number_string = angka.toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? ',' : '';
				rupiah += separator + ribuan.join(',');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
	    }
    
        function calculate() {
       
            const voucher_id = localStorage.getItem('voucherId')

            let body = {
                    "type": "transactions",
                    "attributes": {
                        "voucherId": voucher_id
                    }
                }
        
            $.ajax({
                url: `${baseApi}v1/transactions/calculate`,
                method: "POST",
                headers: {
                'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
                },
                data: body,
                success: function(data) {
                    let res = data.data
                    $('#totalHemat').text(`-${format_rupiah(res.attributes.totalSaving)}`)
                    $('#totalCart').text(`${format_rupiah(res.attributes.totalAll)}`)

            
                },
                error: function(request, textStatus, errorThrown) {
                    const res = JSON.parse(request.responseText)
                    if (res.errors) {
                        const msg = res.errors[0].title
                        flashAlert(msg, 'error')
                        location.reload()
                    }
                }
            })
        }

    </script>
@endpush
