<article id="cart" class="pt-4 pb-1">
    <div class="row">
        @php
            $totalPrice = 0;
        @endphp
        @foreach($carts as $item)
        @php
            $totalPrice += $item->totalPrice;
        @endphp
        <div class="col-12 p-0 mb-4">
            <div class="d-flex flex-row align-items-center">
                <div class="col-4 col-sm-4 pl-0 pr-0">
                    <div class="card item mx-auto">
                        @if($item->variant->product->productimages)
                        <figure>
                            <img src="{{$item->variant->product->productimages[0]->file}}" alt="{{$item->variant->product->name}}">
                        </figure>
                        @endif
                    </div>
                </div>
                <div class="col-5 col-sm-4 px-1 px-sm-0">
                    <h5 class="cart-name">{{$item->variant->product->name}}</h5>
                    <div class="d-flex flex-row">
                        <p class="cart-weight mb-2">{{$item->variant->unitValue .' '. $item->variant->unitTypeName}}  {{!empty($item->pieces) ? "| " .$item->pieces : ''}}</p>
                        <a href="{{route('product.show', $item->variant->product->id)}}">
                            <figure class="d-flex justify-content-end">
                                <img src="{{asset('assets/image/Icons/Edit-active.svg')}}" alt="Edit" class="img-edit">
                            </figure>
                        </a>
                    </div>
                    @if($item->isDiscount)
                    <p class="discount"><span class="coret coretan-{{$item->id}}">Rp {{number_format((int)$item->qty*$item->priceBeforeDiscount)}}</span> 
                    <span class="discount-value px-2">{{$item->savePrice}}</span></p>
                    @endif
                   
                    <p class="cartprice-{{$item->id}}">
                    @if($item->isDiscount)
                    Rp {{number_format((int)$item->qty*$item->priceAfterDiscount)}}
                    @else
                    Rp {{number_format((int)$item->qty*$item->priceBeforeDiscount)}}
                    @endif               
                    </p>
                 
                    
               
                </div>
                <div class="col-3 col-sm-4 pl-0">
                    <form action="" method="post" class="form-qty">
                        <div class="qty">
                            <button 
                            class="qty-minus"
                            data-selector="minus"
                            data-id="{{$item->id}}"
                            data-variant_id="{{$item->variant->id}}"
                            >-</button>
                            <input type="number" readonly class="qty-{{$item->id}}" value="{{$item->qty}}">
                            <button 
                            data-selector="plus" 
                            class="qty-plus"
                            data-id="{{$item->id}}"
                            data-variant_id="{{$item->variant->id}}"
                            >+</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @endforeach
        @php
            $totalPrice = number_format($totalPrice)
        @endphp
    </div>
</article>

@push('customJs')
<script>

    function format_rupiah(angka, prefix = "Rp"){
			var number_string = angka.toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? ',' : '';
				rupiah += separator + ribuan.join(',');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
	}
    $('.total-nominal').text(`Rp {!! $totalPrice !!}`)
    
    $(".qty-minus").add(".qty-plus").on('click', function (e) {
        e.preventDefault();
        
        let cart_id = $(this).data('id')
        let selector = $(this).data('selector')
        let variant_id = $(this).data('variant_id')

        let value = parseInt($('.qty-' + cart_id).val())
        let newVal = 0;

        let valuePrice = parseFloat($('.cartprice-' + cart_id).text().replace(/\D/g, ""));
        let value1Price = valuePrice/value
        let newValPrice = 0;

        let valueCoret = parseFloat($('.coretan-' + cart_id).text().replace(/\D/g, ""));
        let value1Coret = valueCoret/value
        let newValCoret = 0;

        if (selector == 'minus') {
            if (value == 1) {
                if (confirm("Hapus produk dari keranjang belanja ?")) {
                    $('.qty-minus').attr('disabled', true)
                    $('.qty-plus').attr('disabled', true)
                    deleteCart(cart_id)
                }
                return false
            }
            newVal = value - 1
            newValPrice = valuePrice - value1Price
            newValCoret = valueCoret - value1Coret
        
        }

        if (selector == 'plus') {
            newVal = value + 1
            newValPrice = valuePrice + value1Price
            newValCoret = valueCoret + value1Coret
        }

        
        $('.qty-minus').attr('disabled', true)
        $('.qty-plus').attr('disabled', true)
        updateCart(cart_id, variant_id, newVal,selector)
        $(`.qty-${cart_id}`).val(newVal)
        $(`.cartprice-${cart_id}`).text(format_rupiah(newValPrice,"Rp "))
        $(`.coretan-${cart_id}`).text(format_rupiah(newValCoret,"Rp "))

     })

     function deleteCart(cart_id) {
         $.ajax({
             url: baseApi + "v1/carts/"+cart_id,
             method: "DELETE",
             headers: {
                'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
             },
             success: function(data) {
                 location.reload()
                // $('.qty-minus').attr('enabled', true)
                // $('.qty-plus').attr('enables', true)
             }
         })
     }

     function updateCart(cart_id, variant_id, qty, selector) {
         const data = {
             "type": "carts",
             "attributes": {
                 "variantId": variant_id,
                 "qty": qty
             }
         }
         $.ajax({
             url: baseApi + "v1/carts/"+cart_id,
             method: "PUT",
             headers: {
                'Authorization': 'Bearer ' + {!! json_encode(session()->get('token')) !!}
             },
             data: data,
             success: function(data) {
                $('.qty-minus').attr('disabled', false)
                $('.qty-plus').attr('disabled', false)
                calculate()

                if (selector == 'minus') {
                    var countCart = $('#countCart').text()
                    $('#countCart').text(parseInt(countCart)-1)
                }else{
                    var countCart = $('#countCart').text()
                    $('#countCart').text(parseInt(countCart)+1)
                }
                //  location.reload()
             },
             error: function(request, textStatus, errorThrown) {
                const res = JSON.parse(request.responseText)
                if(res.errors) {
                    const msg = res.errors[0].title
                    flashAlert(msg, 'error')
                    location.reload()
                }
            }
         })
     }
</script>
@endpush