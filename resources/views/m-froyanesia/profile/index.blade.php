@extends('m-froyanesia.layouts.master')
@section('page')
    Profil
@endsection
@section('content')
    <header>
        <nav class="navbar navbar-expand">
            <div class="container px-3 py-2">
                <div class="d-flex justify-content-between w-100">
                    <a href="/" class="title-page"><i class="fas fa-chevron-left"></i></a>
                    <h6 class="title-page pl-4 pt-2 mx-auto">Profil</h6>
                    <a href="/profile/ubah" class="edit">
                        <figure class="p-0 m-0 text-center">
                            <img src="{{asset('assets/image/Icons/Edit.svg')}}" alt="Edit" class="rounded">
                        </figure>
                    </a>
                </div>
            </div>
        </nav>
    </header>
    <main>
        <article id="profile" class="p-3">
            <div class="container">
                <section id="photo" class="mb-4">
                    <figure class="text-center">
                        @php
                            $photo = $data['attributes']['photo'] ?? asset('assets/image/no-photo.png');
                        @endphp
                    <img src="{{$photo}}" id="photo-profil" class="img-profil rounded">
                        {{-- <figcaption class="mt-2">ID : "{{$data['id']}}</figcaption> --}}
                    </figure>
                </section>
                <section id="name" class="mb-4">
                    <h6 class="profile-title">Nama</h6>
                    <div class="card border-0 p-3">
                        <p class="profile-content">{{$data['attributes']['name']}}</p>
                    </div>
                </section>
                <section id="email" class="mb-4">
                    <h6 class="profile-title">No. Whatsapp</h6>
                    <div class="card border-0 p-3">
                        <p class="profile-content">{{$data['attributes']['email'] ?? $data['attributes']['phone']}}</p>
                    </div>
                </section>
                {{-- <section id="password" class="mb-4">
                    <h6 class="profile-title">Password</h6>
                    <div class="card border-0 p-3">
                        <div class="d-flex justify-content-between">
                            <p id="text-password" class="profile-content">password</p>
                            <img src="{{asset('assets/image/Icons/Tutup Mata.svg')}}" alt="Hide" class="img-fluid">
                        </div>
                    </div>
                </section> --}}
                <section id="alamat" class="mb-4">
                    <h6 class="profile-title">Alamat</h6>
                    <div class="card border-0 p-3">
                        <p class="profile-content">{{$data['attributes']['address']}}</p>
                    </div>
                </section>
                <section id="gender" class="mb-4">
                    <h6 class="profile-title">Jenis Kelamin</h6>
                    <div class="card border-0 p-3">
                        <p class="profile-content">{{$data['attributes']['gender']}}</p>
                    </div>
                </section>
                <section id="ktp" class="mb-4">
                    <h6 class="profile-title">No. KTP</h6>
                    <div class="card border-0 p-3">
                        <p class="profile-content">{{$data['attributes']['identityNumber']}}</p>
                    </div>
                </section>
                <section id="tgl_lahir" class="mb-4">
                    <h6 class="profile-title">Tanggal Lahir</h6>
                    <div class="card border-0 p-3">
                        <p class="profile-content">{{date('d/m/Y', strtotime($data['attributes']['bornDate']))}}</p>
                    </div>
                </section>
                <section id="tgl_gabung" class="mb-4">
                    <h6 class="profile-title">Tanggal Gabung</h6>
                    <div class="card border-0 p-3">
                        <p class="profile-content">{{date('d/m/Y', strtotime($data['attributes']['joinDate']))}}</p>
                    </div>
                </section>
            </div>
        </article>
    </main>
    <footer>
        @include('m-froyanesia.partials.bot-nav')
    </footer>
@endsection
