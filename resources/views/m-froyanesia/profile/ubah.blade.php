@extends('m-froyanesia.layouts.master')
@section('page')
Profil
@endsection
@push('customCss')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush

@section('content')
<header>
    <nav class="navbar navbar-expand">
        <div class="container px-3 py-2">
            <h6 class="title-page pt-2 mx-auto">Profil</h6>
        </div>
        </div>
    </nav>
</header>
<main>
    <article id="profileEdit" class="p-3">
        <div class="container">
            <form action="{{route('profile.update')}}" method="post" class="edit-profile" enctype="multipart/form-data">
                @csrf
                <figure class="text-center mx-auto">
                    @php
                        $photo = $data['attributes']['photo'] ?? asset('assets/image/no-photo.png');
                    @endphp
                    <img src="{{$photo}}" id="photo-profil" class="img-profil-edit rounded">
                    <img src="{{asset('assets/image/Icons/Edit-foto.svg')}}" alt="Ubah Foto" class="edit-foto">
                    <input type="file" name="photo" id="input-foto" onchange="uploadPhoto(this);" accept="image/*">
                    {{-- <figcaption class="mt-2">ID : {{$data['id']}}</figcaption> --}}
                </figure>
                @if($errors->any())
                <h4>{{$errors->first()}}</h4>
                @endif
                <input type="text" class="input-custom mb-4" value="{{$data['id']}}" name="id" hidden>
                <label for="name" class="custom--label">Nama</label>
                <input type="text" name="name" class="input-custom mb-4" value="{{$data['attributes']['name']}}">
                <label for="emailOrPhone" class="custom--label">No. Whatsapp</label>
                <input type="text" name="emailOrPhone" class="input-custom mb-4"
                    value="{{$data['attributes']['email'] ?? $data['attributes']['phone']}}" readonly>
                <label for="password" class="custom--label">Password</label>
                <div class="input-group-custom mb-4">
                    <input type="password" name="password" id="password" class="input-custom">
                    <span toggle="#password" id="toggle-password" class="fas fa-eye"></span>
                </div>
                <label for="search_address" class="custom--label">Alamat</label>
                <input type="text" class="input-custom mb-2 autoComplete" id="search_address">
                
                <input type="hidden" name="villageId" id="villageId" value="{{$data['relationships']['village']['data']['id']}}">
                <textarea class="input-custom py-2 mb-4" name="address" id="" placeholder="detail alamat">{{$data['attributes']['address']}}</textarea>
                <label for="gender" class="custom--label">Jenis Kelamin</label>
                <select name="gender" class="form-control">
                    <option value="Pria" {{$data['attributes']['gender'] == "Pria" ? 'selected' : ''}}>Pria</option>
                    <option value="Wanita"{{$data['attributes']['gender'] == "Wanita" ? 'selected' : ''}}>Wanita</option>
                </select>
                <label for="ktp" class="custom--label">No. KTP</label>
                <input type="text" name="identityNumber" class="input-custom mb-4" value="{{$data['attributes']['identityNumber']}}">
                <label for="birthday" class="custom--label">Tanggal Lahir</label>
                <input type="date" name="bornDate" class="input-custom mb-4" name="{{date("Y-m-d", strtotime($data['attributes']['bornDate']))}}">
                <div class="card join border-0 p-3">
                    <p class="join-date">{{$data['attributes']['joinDate']}}</p>
                </div>
                <div class="col-7 ml-auto p-0 mt-4">
                    <div class="row">
                        <div class="col-6 pr-1">
                            <a href="/profile">
                                <button class="btn-cancel">Batal</button>
                            </a>
                        </div>
                        <div class="col-6 pl-1">
                            <button type="submit" class="btn-save">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </article>
</main>
@endsection
@push('customJs')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    function uploadPhoto (input) {
        if (input.files && input.files[0]) {
            
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#photo-profil').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            removeFoto()
        }
    }

    $( ".autoComplete" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: baseApi + `v1/villages?limit=999&sort=asc&include=district`,
                data: {
                    search: request.term
                },
                success: function( data ) {
                    let res = data.data.map((value, key) => {
                        result = {
                            "name": value.attributes.name,
                            "id": value.id
                        }
                        let included = data.included
                        
                        if(typeof value.relationships !== 'undefined' && typeof included[key] !== 'undefined') {
                            result['district'] = included[key].attributes.name
                        }
                        return result
                    }) 
                    response( res );
                }
            });
        },
        minLength: 3,
        select: function( event, ui ) {
            let villageId = ui.item.id
            $('#villageId').val(villageId)
            let label = ui.item.name +", " + ui.item.district
            $( ".autoComplete" ).val(label);
            return false;
        },
        open: function() {
            $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function() {
            $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( `<div>${item.name + ', ' + item.district}</div>`)
        .appendTo( ul );
    };
</script>
@endpush