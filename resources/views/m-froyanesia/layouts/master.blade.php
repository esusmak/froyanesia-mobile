<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keyword" content="e-commerce">
    <meta name="author" content="Froyanesia">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('assets/library/bootstrap-4.5.3-dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/library/fontawesome/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/library/datetimepicker/css/datetimepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/library/slick-1.8.1/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assets/library/slick-1.8.1/slick/slick-theme.css')}}">
    {{-- <link href='https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.css' rel='stylesheet' />
    <link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.1.0/mapbox-gl-directions.css"/> --}}
    <link rel="stylesheet" href="{{asset('assets/css/styles.min.css')}}">
    <title>@yield('page') | Froyanesia</title>
    @stack('customCss')
</head>
<body>
    {{-- @include('m-froyanesia.partials.preloader') --}}
    @yield('content')
    <script src="{{asset('assets/library/jquery/jquery-3.4.1.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.6.0/umd/popper.min.js" integrity="sha512-BmM0/BQlqh02wuK5Gz9yrbe7VyIVwOzD1o40yi1IsTjriX/NGF37NyXHfmFzIlMmoSIBXgqDiG1VNU6kB5dBbA==" crossorigin="anonymous"></script>
    <script src="{{asset('assets/library/bootstrap-4.5.3-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
    <script src="{{asset('assets/library/datetimepicker/js/datetimepicker.min.js')}}"></script>
    <script src="{{asset('assets/library/slick-1.8.1/slick/slick.min.js')}}"></script>
    {{-- <script src='https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.js'></script>
    <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.1.0/mapbox-gl-directions.js"></script> --}}
    <script src="{{asset('assets/js/theme.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script
    src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
    <script>
        var baseApi = `{!! env('BASE_URI_API') !!}`
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ajaxStart(function () {
            $.LoadingOverlay("show");
        });

        $(document).ajaxComplete(function () {
            $.LoadingOverlay("hide");
        });

        function flashAlert(msg, typeMsg) {
            Swal.fire({
                title: msg,
                icon: typeMsg,
                confirmButtonText: 'Ok'
            })
        }
    </script>
    @if(session()->has('message'))
    <script>
        msg = `{!!session()->get('message')!!}`
        flashAlert(msg, 'info')
    </script>
    @endif
    @stack('customJs')
</body>
</html>
