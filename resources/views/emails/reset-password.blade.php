@component('mail::message')

Silahkan Klik Tautan Dibawah ini untuk Reset password :

@component('mail::panel')
<a href="{{route('public.auth.resetPassword', $token)}}" class="btn btn-primary">Reset password</a>
@endcomponent

Thanks,<br>
{{ env('APP_NAME') }}
@endcomponent
