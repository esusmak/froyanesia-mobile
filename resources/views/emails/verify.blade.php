@component('mail::message')
#Dear User Baru,

Silahkan Klik Tautan Dibawah ini untuk verifikasi email :

@component('mail::panel')
<a href="{{route('public.auth.verifyEmail', $token)}}" class="btn btn-primary">Aktivasi</a>
@endcomponent

Thanks,<br>
{{ env('APP_NAME') }}
@endcomponent
