<?php

return [
    'api_host' => 'https://console.zenziva.net/',
    'user_key' => env('ZENVIVA_USER_KEY'),
    'pass_key' => env('ZENVIVA_API_KEY'),
];
